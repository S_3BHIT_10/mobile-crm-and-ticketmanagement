//var db = null;

//Wait for Cordova to load
document.addEventListener('deviceready', onDeviceReady, false);

//Cordova is ready
function onDeviceReady(){
	var db = window.sqlitePlugin.openDatabase({name:'demo.db', location: 'default'});
	db.transaction(pupulateDB, errorCB, successCB);
}

//Create DB
function pupulateDB(tx)
{
	//Tabellen l�schen falls vorhanden
	tx.executeSql('DROP TABLE IF EXISTS benutzer');
	tx.executeSql('DROP TABLE IF EXISTS klient');
	tx.executeSql('DROP TABLE IF EXISTS ansprechperson');
	tx.executeSql('DROP TABLE IF EXISTS kunde');
	tx.executeSql('DROP TABLE IF EXISTS kontaktart');
	tx.executeSql('DROP TABLE IF EXISTS projektstamm');
	tx.executeSql('DROP TABLE IF EXISTS aufzeichnung_leistung');
	tx.executeSql('DROP TABLE IF EXISTS aufzeichnung_diaet');
	
	//Tabellen erstellen
	tx.executeSql('CREATE TABLE IF NOT EXISTS klient(MDKND.KLIENT_ID INTEGER NOT NULL PRIMARY KEY, bezeichnung text)');
	tx.executeSql('CREATE TABLE IF NOT EXISTS kunde(MDKND.KND_ID INTEGER NOT NULL PRIMARY KEY, MDKND.MATCH text, MDKND.ZUNAME text, MDKND.NAME2 text, MDKND.ORT text, MDKND.STRASSE text, MDKND.PLZ text, MDKND.TELEFON text, FOREIGN KEY(MDKND.KLIENT_ID) references klient(MDKND.KLIENT_ID))');
	tx.executeSql('CREATE TABLE IF NOT EXISTS benutzer(b_id INTEGER NOT NULL PRIMARY KEY, username text, passwort text, webservice_endpoint text, verschluesselungskey text)');
	tx.executeSql('CREATE TABLE IF NOT EXISTS kontaktart(MDLST.LST_ID INTEGER NOT NULL PRIMARY KEY, MDLST.BEZEICHNUNG text, FOREIGN KEY(MDKND.KLIENT_ID) references klient(MDKND.KLIENT_ID))');
	tx.executeSql('CREATE TABLE IF NOT EXISTS projektstamm(MDPRJ.PRJ_ID INTEGER NOT NULL PRIMARY KEY, MDPRJ.BEZEICHNUNG text, FOREIGN KEY(MDKND.KND_ID) references kunde(MDKND.KND_ID), FOREIGN KEY(MDKND.KLIENT_ID) references klient(MDKND.KLIENT_ID))');
	tx.executeSql('CREATE TABLE IF NOT EXISTS ansprechperson(MDZHD.SID INTEGER NOT NULL PRIMARY KEY, MDZHD.VORNAME text, MDZHD.ZUNAME text, MDZHD.EMAIL text, MDZHD.MOBIL text, MDZHD.MOBIL_PRIVAT text, FOREIGN KEY(MDKND.KND_ID) references kunde(MDKND.KND_ID),FOREIGN KEY(MDKND.KLIENT_ID) references klient(MDKND.KLIENT_ID))');
	tx.executeSql('CREATE TABLE IF NOT EXISTS aufzeichnung_leistung(A_L_ID INTEGER NOT NULL PRIMARY KEY, FOREIGN KEY(MDKND.KLIENT_ID) references klient(MDKND.KLIENT_ID), MDKND.KND_ID INTEGER NOT NULL, MDZHD.SID INTEGER NOT NULL, MDLST.LST_ID INTEGER NOT NULL, MDPRJ.PRJ_ID INTEGER NOT NULL, DATUM_VON datetime, DATUM_BIS datetime, TITEL text, NOTIZ text, USERNAME text, aufzeichnungsart text)');
	tx.executeSql('CREATE TABLE IF NOT EXISTS aufzeichnung_diaet(A_D_ID INTEGER NOT NULL PRIMARY KEY, FOREIGN KEY(MDKND.KLIENT_ID) references klient(MDKND.KLIENT_ID), MDLST.LST_ID INTEGER NOT NULL, MDPRJ.PRJ_ID INTEGER NOT NULL, KILOMETER text, DATUM_VON datetime, DATUM_BIS datetime, MDKND.KND_ID INTEGET NOT NULL, NOTIZ text, USERNAME text, aufzeichnungsart text)');

	//Werte in die DB einf�gen
	tx.executeSql('INSERT INTO benutzer(b_id, username, passwort, webservice_endpoint, verschluesselungskey) VALUES(1, "Martin", "12345", "6789", "0000")');
	
}

//Transaction error callback
function errorCB(tx, err)
{
	alert("SQL Errror" + err);
}

//Transaction success callback
function successCB(){
	alert("Success");
}




	