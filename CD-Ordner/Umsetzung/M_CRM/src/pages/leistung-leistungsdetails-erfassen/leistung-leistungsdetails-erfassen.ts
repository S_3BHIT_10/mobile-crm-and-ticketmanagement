import { Component } from '@angular/core';
import {NavController, NavParams, Platform} from 'ionic-angular';
import { SQLite } from "ionic-native";
/*
  Generated class for the LeistungLeistungsdetailsErfassen page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-leistung-leistungsdetails-erfassen',
  templateUrl: 'leistung-leistungsdetails-erfassen.html'
})
export class LeistungLeistungsdetailsErfassenPage {
  //ansprechperson
  contactperson_sid: any;
  contactperson_vorname: any;
  contactperson_nachname: any;
  //kontaktart
  contactTypeList: any[] = [];
  selectedContactType: any;
  //projektstamm
  project_prj_id: any;
  project: any[] = [];
  //kunde
  customer_knd_id: any;
  customer_zuname: any;
  customer_name2: any;
  //klient
  customer_klient_id: any;
  //user
  mduser_userid: any;
  //aufzeichnung_leistung
  recordingtype: any;
  selectedrecordingtype: String ='Vertrieb';
  notes: string='';
  title: any;
  synchronized: number = 0;

  //date and duration
  //string ist kleingeschrieben da wir für Date.parse unten den einfachen Datentyp benötigen
  public startDate: string;
  public duration: string;
  public endDate: string;

  //database
  database: SQLite;
  showleistung: Object[];


  constructor(public navCtrl: NavController, public navParams: NavParams, private platform: Platform) {
    this.customer_knd_id = navParams.get("knd_id");
    this.customer_zuname = navParams.get("zuname");
    this.customer_name2 = navParams.get("name2");
    this.customer_klient_id = navParams.get("klient_id");
    this.contactperson_sid = navParams.get("sid");
    this.contactperson_vorname = navParams.get("contactpersongivenname");
    this.contactperson_nachname = navParams.get("contactpersonsurname");
    this.platform.ready().then(() => {
      this.database = new SQLite();
      this.database.openDatabase({name: "data.db", bgType: '1', iosDatabaseLocation: 'Library'}).then(() => {
          this.getStandardDuration();
          this.captureContactType();
          this.captureProject();
          this.selectUserID();
          this.pushRecordingtype();
      }, (error) => {
        console.log("Database ERROR: ", error);
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LeistungLeistungsdetailsErfassenPage');
  }

  //Selektiert die Standarddauer einer Leistung
  getStandardDuration(){
    this.database.executeSql("SELECT PERFORMANCEDURATION FROM MDUSER WHERE U_ID=1", []).then((data) => {
      if (data.rows.length > 0) {
        this.duration=new Date(data.rows.item(0).PERFORMANCEDURATION).toISOString();
        this.startDate=new Date(new Date(new Date().setMinutes(new Date().getMinutes()-new Date(this.duration).getMinutes())).setHours(new Date().getHours()+1-(new Date(this.duration).getHours()-1))).toISOString();
        console.log("standardDurationLeistungen: "+ new Date(this.duration).toISOString());
      }
    }, (error) => {
      alert("ERROR loading diet records: " + JSON.stringify(error));
    });
  }

  //Beinhaltet die Aufzeichnungsarten einer Leistung
  pushRecordingtype(){
    this.recordingtype = [];
    this.recordingtype.push({value: 'Ticket', text: 'Ticket',  selected: false});
    this.recordingtype.push({value: 'Vertrieb', text: 'Vertrieb', selected: true});
  }

  //Selektiert alle Kontaktarten
  captureContactType() {
    this.database.executeSql("SELECT LST_ID, BEZEICHNUNG FROM MDLST WHERE KLIENT_ID ="+this.customer_klient_id, []).then((data) => {
      this.selectedContactType = data.rows.item(0).LST_ID;
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          this.contactTypeList.push({LST_ID: data.rows.item(i).LST_ID, BEZEICHNUNG: data.rows.item(i).BEZEICHNUNG});
        }
      }
    }, (error) => {
      alert("ERROR capturing contact type: " + JSON.stringify(error));
    });
  }

  //Selektiert alle Projekte eines Kunden
  captureProject() {
    this.database.executeSql("SELECT PRJ_ID, BEZEICHNUNG FROM MDPRJ WHERE KND_ID ="+this.customer_knd_id+" ORDER BY BEZEICHNUNG", []).then((data) => {
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          this.project.push({PRJ_ID: data.rows.item(i).PRJ_ID, BEZEICHNUNG: data.rows.item(i).BEZEICHNUNG});
          //this.project_bezeichnung = data.rows.item(i).BEZEICHNUNG;
          //this.project_prj_id = data.rows.item(i).PRJ_ID;
        }
      }
      //alert(this.project);
    }, (error) => {
      alert("ERROR capturing project: " + JSON.stringify(error));
    });
  }

  //Ändert die Kontakart im UI
  changeContacttype(selectedValue)
  {
    this.selectedContactType = selectedValue;
  }

  //Ändert das Projekt im UI
  changeProject(selectedValue)
  {
    this.project_prj_id = selectedValue;

  }

  //Selektiert die User_ID des Benutzer, um die Daten einzuspeichern
  selectUserID()
  {
    this.database.executeSql("SELECT USER_ID FROM MDUSER WHERE KLIENT_ID ="+this.customer_klient_id, []).then((data) => {
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          this.mduser_userid = data.rows.item(i).USER_ID;
        }
      }
    }, (error) => {
      alert("ERROR capturing user_id: " + JSON.stringify(error));
    });
  }

  //Speichert die eingebenen Daten als Leistung ab
  insertData()
  {
    var start = Date.parse(this.startDate);
    var dur = Date.parse(this.duration);
    var time = new Date(dur).getTime();

    this.endDate=new Date(new Date(start).getTime()+time).toISOString();
    //Dauer in Sekunden
    //this.duration = new Date(time).getTime()/1000;
    this.duration = new Date(time).toISOString();


    var insertquery = "INSERT INTO INKNDLST (KLIENT_ID, KND_ID, SID, LST_ID, PRJ_ID, DATUM_VON, DATUM_BIS, DAUER, TITEL, NOTIZ, USER_ID, AUFZEICHNUNGSART, SYNCHRONIZED) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    this.database.executeSql(insertquery, [this.customer_klient_id, this.customer_knd_id, this.contactperson_sid, this.selectedContactType, this.project_prj_id, this.startDate, this.endDate, this.duration, this.title, this.notes, this.mduser_userid, this.selectedrecordingtype, this.synchronized]).then((data) => {
      console.log("INSERTED: " + JSON.stringify(data));
    }, (error) => {
      alert("ERROR: " + JSON.stringify(error));
    });
    this.navCtrl.popToRoot();
  }

  //Leitet Benutzer zur Startseite zurück
  goBack(){
    this.navCtrl.popToRoot();
  }
}
