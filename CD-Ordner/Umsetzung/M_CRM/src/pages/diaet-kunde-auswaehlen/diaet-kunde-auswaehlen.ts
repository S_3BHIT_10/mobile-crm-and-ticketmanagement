import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { DiaetErfassenPage } from "../diaet-erfassen/diaet-erfassen";
import { SQLite } from "ionic-native";
/*
 Generated class for the DiaetKundeAuswaehlen page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-diaet-kunde-auswaehlen',
  templateUrl: 'diaet-kunde-auswaehlen.html'
})
export class DiaetKundeAuswaehlenPage {
  database: SQLite;
  customers: Object[];
  customersAll: Object[];
  searchString: String;
  constructor(public navCtrl: NavController, public navParams: NavParams, private platform: Platform) {
    this.platform.ready().then(() => {
      this.database = new SQLite();
      this.database.openDatabase({name: "data.db", bgType: '1', iosDatabaseLocation: 'Library'}).then(() => {
        this.initializeItems();
      }, (error) => {
        console.log("ERROR: ", error);
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DiaetKundeAuswaehlenPage');
  }

  //Selektiert Kundeninformation, die für die Searchbar benötigt werden, sowie für die Diät selbst
  initializeItems() {
    this.database.executeSql("SELECT KND_ID, MATCH_KND, ZUNAME, NAME2, ORT, PLZ, KlIENT_ID FROM MDKND ORDER BY ZUNAME, NAME2 ASC ", []).then((data) => {
      this.customers = [];
      if(data.rows.length > 0) {
        for(var i = 0; i < data.rows.length; i++) {
          this.customers.push({
            KND_ID: data.rows.item(i).KND_ID,
            MATCH_KND: data.rows.item(i).MATCH_KND,
            ZUNAME: data.rows.item(i).ZUNAME,
            NAME2: data.rows.item(i).NAME2,
            ORT: data.rows.item(i).ORT,
            PLZ: data.rows.item(i).PLZ,
            KLIENT_ID: data.rows.item(i).KLIENT_ID
          });
        }
        this.customersAll=this.customers;
      }
    }, (error) => {
      alert("Error loading customer: " + JSON.stringify(error));
    });
  }

  //Suchfunktion für die Searchbar
  //siehe "Sonstige_Codequellen_MP"
  getItems(ev: any) {
    // Reset items back to all of the items
    this.customers=this.customersAll;

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.customers = this.customers.filter((item) => {
        this.searchString=item["ZUNAME"]+item["NAME2"]+item["MATCH_KND"];
        return (this.searchString.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  //Sendet Daten an die "diaet-erfassen"-Page
  itemSelected(item:string){
    this.navCtrl.push(DiaetErfassenPage, {
      knd_id: item["KND_ID"],
      zuname: item["ZUNAME"],
      name2: item["NAME2"],
      klient_id: item["KLIENT_ID"]
    });
  }
}
