import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import {LeistungLeistungsdetailsErfassenPage} from "../leistung-leistungsdetails-erfassen/leistung-leistungsdetails-erfassen";
import { SQLite } from "ionic-native";

/*
 Generated class for the LeistungErfassen page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-leistung-erfassen',
  templateUrl: 'leistung-erfassen.html'
})
export class LeistungErfassenPage {
  database: SQLite;
  contactperson: Object[];
  contactpersonAll: Object[];
  searchString: String;

  constructor(public navCtrl: NavController, public navParams: NavParams, private platform: Platform) {
    this.platform.ready().then(() => {
      this.database = new SQLite();
      this.database.openDatabase({name: "data.db", bgType: '1', iosDatabaseLocation: 'Library'}).then(() => {
        this.initializeItems();
      }, (error) => {
        console.log("ERROR: ", error);
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LeistungErfassenPage');
  }

  //Selektiert alle Ansprechpersonen und Kunden
  initializeItems() {
    this.database.executeSql("SELECT k.KND_ID AS KND_ID, k.ZUNAME AS ZUNAME, k.NAME2 AS NAME2, k.KlIENT_ID AS KLIENT_ID, " +
        "z.SID AS SID, z.KND_ID, z.KLIENT_ID, z.VORNAME AS ZVORNAME, " +
        "z.ZUNAME AS ZZUNAME, k.MATCH_KND AS KMATCH_KND " +
        "FROM MDKND k, MDZHD z " +
        "WHERE k.KND_ID = z.KND_ID and k.KLIENT_ID = z.KLIENT_ID", []).then((data) => {
      this.contactperson = [];
      if(data.rows.length > 0) {
        for(var i = 0; i < data.rows.length; i++) {
          this.contactperson.push({
            KND_ID: data.rows.item(i).KND_ID,
            ZUNAME: data.rows.item(i).ZUNAME,
            NAME2: data.rows.item(i).NAME2,
            KLIENT_ID: data.rows.item(i).KLIENT_ID,
            CONTACTPERSONGIVENNAME: data.rows.item(i).ZVORNAME,
            CONTACTPERSONSURNAME: data.rows.item(i).ZZUNAME,
            SID: data.rows.item(i).SID,
            MATCH_KND: data.rows.item(i).KMATCH_KND});
        }
        this.contactpersonAll=this.contactperson;
      }
    }, (error) => {
      alert("ERROR loading contactperson: " + JSON.stringify(error));
    });
  }

  //Suchfunktion für die Searchbar
  getItems(ev: any) {
    // Reset items back to all of the items
    this.contactperson=this.contactpersonAll;

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.contactperson = this.contactperson.filter((item) => {
        this.searchString=item["CONTACTPERSONGIVENNAME"]+item["CONTACTPERSONSURNAME"];
        return (this.searchString.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  //übergibt relevante Daten an "leistung-leistungsdetails-erfassen"-Page
  itemSelected(item:string){
    this.navCtrl.push(LeistungLeistungsdetailsErfassenPage, {
      knd_id: item["KND_ID"],
      zuname: item["ZUNAME"],
      name2: item["NAME2"],
      klient_id: item["KLIENT_ID"],
      sid: item["SID"],
      contactpersongivenname: item["CONTACTPERSONGIVENNAME"],
      contactpersonsurname: item["CONTACTPERSONSURNAME"]
    });
  }
}