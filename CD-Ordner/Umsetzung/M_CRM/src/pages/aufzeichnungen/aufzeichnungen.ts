import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ModalController, AlertController, LoadingController,Loading} from 'ionic-angular';
import { SQLite} from "ionic-native";
import { EditLModalPage} from "../edit-l-modal/edit-l-modal";
import { EditDModalPage} from "../edit-d-modal/edit-d-modal";

declare function RequestPing(url:string);
declare function RequestLogin(url:string, db:string, user:string, pwd:string, klientId:string, owner:string, ownerPwd:string);
declare function RequestLogout(url:string, sessionGuid:string);
declare function RequestReadData(url:string, sessionGuid:string, commandName:string);
declare function RequestCreateData( url:string, sessionGuid:string, commandName:string, createData:string)
/*
 Generated class for the Aufzeichnungen page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-aufzeichnungen',
  templateUrl: 'aufzeichnungen.html'
})
export class AufzeichnungenPage {
  //Synchronisieren
  testCheckboxResult;
  //DB
  database: SQLite;
  //Aufzeichnungen
  Aufzeichnungen: string = "Leistungen";
  records: Object[];
  synchronizedrecords: Object[] = [];
  dietrecords: Object[];
  connectionStatus: string;
  connection: Boolean;
  syncButtonClicked: Boolean;

  //LoginData
  url:any;
  db:any = "SUP12";
  user:any;
  pwd:any;
  klient_id:any;
  owner:any;
  ownerPwd:any;
  db_id:any;
  alert:any;
  loader:any;

  //counter MDLST, MDZHD
  mdlstcounter: number = 0;
  mdzhdcounter: number = 0;

  //sessionValue
  sessionValue:any = '';

  //setSynchronized
  synchronized: number = 1;
  allSynchronizedRecords:any;
  synchronizeddate: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public alertCtrl: AlertController, public modalCtrl: ModalController, public loadingCtrl: LoadingController) {
    this.platform.ready().then(() => {
      this.database = new SQLite();
      this.database.openDatabase({name: "data.db", bgType: '1', iosDatabaseLocation: 'Library'}).then(() => {
        this.LoadMDUSERData();
        this.initializeRecords();
        this.initializeDietRecords();
        this.initializeSyncedRecords();
      }, (error) => {
        console.log("ERROR: ", error);
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AufzeichnungenPage');
  }

  //Dient zum Laden der synchroniserten Einträge
  initializeSyncedRecords(){
    console.log("initizalizeSyncedRecords open");
    this.synchronizedrecords=[];
    this.database.executeSql("SELECT EXISTS(SELECT l.SYNCHRONIZED, d.SYNCHRONIZED FROM INKNDLST l, INDIAETEN d) AS RESULT, EXISTS(SELECT l.SYNCHRONIZED FROM INKNDLST l) AS L_RESULT, EXISTS(SELECT d.SYNCHRONIZED FROM INDIAETEN d) AS D_RESULT",[]).then((syncDataResult)=> {
      if (syncDataResult.rows.item(0).RESULT != 0 || syncDataResult.rows.item(0).L_RESULT != 0 || syncDataResult.rows.item(0).D_RESULT != 0) {

        this.synchronizedrecords = [];
        this.database.executeSql(
            "SELECT l.A_L_ID, l.SID, l.KLIENT_ID, l.TITEL as TITLE, l.PRJ_ID, l.LST_ID, l.AUFZEICHNUNGSART as RECORDTYPE, l.DATUM_VON as DATE_FROM, l.SYNCHRONIZED as SYNCHRONIZED, z.SID, z.VORNAME as GNCONTACTPERSON, z.ZUNAME as SNCONTACTPERSON, p.PRJ_ID, p.BEZEICHNUNG as PROJECTNAME " +
            "FROM INKNDLST l " +
            "LEFT JOIN MDZHD z ON l.SID = z.SID " +
            "LEFT JOIN MDPRJ p ON l.PRJ_ID = p.PRJ_ID " +
            "WHERE l.SYNCHRONIZED > 0 ", []).then((data)=> {

          if (data.rows.length > 0) {
            for (var i = 0; i < data.rows.length; i++) {
              console.log("Leisutngs: " + data.rows.item(i));
              var sqlDate = data.rows.item(i).DATE_FROM.toString();
              var splittedStartDate = this.splitDateTime(sqlDate);

              this.synchronizedrecords.push({
                A_L_ID: data.rows.item(i).A_L_ID,
                CONTACTPERSONGIVENNAME: data.rows.item(i).GNCONTACTPERSON,
                CONTACTPERSONSURNAME: data.rows.item(i).SNCONTACTPERSON,
                TITLE: data.rows.item(i).TITLE,
                PROJECTNAME: data.rows.item(i).PROJECTNAME,
                RECORDTYPE: data.rows.item(i).RECORDTYPE,
                DATE_FROM: data.rows.item(i).DATE_FROM,
                DATE_FROM1: splittedStartDate.sDay + "." + splittedStartDate.sMonth + "." + splittedStartDate.sYear + ", " + splittedStartDate.sHour + ":" + splittedStartDate.sMinute,
                LST_ID: data.rows.item(i).LST_ID
              });
            }
          }

        });
        this.database.executeSql(
            "SELECT d.A_D_ID, d.KLIENT_ID, d.LST_ID, d.KND_ID, d.AUFZEICHNUNGSART as RECORDTYPE, d.TITEL as TITLE, d.KILOMETER, d.DATUM_VON as DATE_FROM, d.SYNCHRONIZED, k.KLIENT_ID, k.KND_ID, k.ZUNAME as GNCUSTOMER, k.NAME2 as SNCUSTOMER " +
            "FROM INDIAETEN d " +
            "LEFT JOIN MDKND k ON d.KLIENT_ID = k.KLIENT_ID AND d.KND_ID = k.KND_ID " +
            "WHERE d.SYNCHRONIZED > 0", []).then((data)=> {
          for (var i = 0; i < data.rows.length; i++) {

            console.log("Diaeten: " +data.rows.item(i));

            var sqlDate = data.rows.item(i).DATE_FROM.toString();
            var splittedStartDate = this.splitDateTime(sqlDate);

            this.synchronizedrecords.push({
              A_D_ID: data.rows.item(i).A_D_ID,
              GIVENNAME: data.rows.item(i).GNCUSTOMER,
              SURNAME: data.rows.item(i).SNCUSTOMER,
              RECORDTYPE: data.rows.item(i).RECORDTYPE,
              KILOMETER: data.rows.item(i).KILOMETER,
              TITLE: data.rows.item(i).TITLE,
              DATE_FROM1: splittedStartDate.sDay + "." + splittedStartDate.sMonth + "." + splittedStartDate.sYear + ", " + splittedStartDate.sHour + ":" + splittedStartDate.sMinute,
              DATE_FROM: data.rows.item(i).DATE_FROM,
              LST_ID: data.rows.item(i).LST_ID
            });
          }
          this.allSynchronizedRecords=this.synchronizedrecords;
        });

        /***********************************************************
         Die Sortiermethode funktioniert noch nicht, wird jedoch
         für die weitere Entwicklung dieser Applikation nicht
         entfernt.
         **********************************************************/
        /*
        var fertig=false;
        while(fertig==false)
        {
          for(var i=0;i<1;i++)
          {
            var erg=this.allSynchronizedRecords[i]["DATE_FROM"]-this.allSynchronizedRecords[i+1]["DATE_FROM"];
            if(erg>0)
            {
              console.log("erste if");
            }
            if(erg<0)
            {
              var zwischenwert = this.allSynchronizedRecords[i]["DATE_FROM"];
              this.allSynchronizedRecords[i]["DATE_FROM"]=this.allSynchronizedRecords[i+1]["DATE_FROM"];
              this.allSynchronizedRecords[i+1]["DATE_FROM"]=zwischenwert;
            }
            if(erg==0)
            {

            }
          }
        }
        */
      }

    });
  }

  //Dient zum Laden der Leistungseinträge
  initializeRecords() {
    this.database.executeSql("SELECT a.A_L_ID, a.KLIENT_ID as aKLIENT_ID, a.KND_ID as aKND_ID, a.SID as SID, a.LST_ID as LST_ID, a.PRJ_ID as PRJ_ID, a.DATUM_VON AS STARTDATE, a.DATUM_BIS as ENDDATE, a.DAUER AS DURATION, a.TITEL, a.NOTIZ as NOTES, a.USER_ID as U_ID, a.AUFZEICHNUNGSART AS RECORDTYPE, a.SYNCHRONIZED AS SYNC, k.KLIENT_ID, k.KND_ID, k.ZUNAME AS GNCUSTOMER, k.NAME2 AS SNCUSTOMER, z.SID, z.VORNAME AS GNCONTACTPERSON, z.ZUNAME AS SNCONTACTPERSON, p.PRJ_ID, p.BEZEICHNUNG AS PROJECTNAME " +
        "FROM INKNDLST a " +
        "LEFT JOIN MDKND k ON a.KLIENT_ID = k.KLIENT_ID AND a.KND_ID = k.KND_ID " +
        "LEFT JOIN MDZHD z ON a.SID = z.SID " +
        "LEFT JOIN MDPRJ p ON a.PRJ_ID = p.PRJ_ID " +
        "WHERE a.SYNCHRONIZED = 0", []).then((data) => {

      if (data.rows.length > 0) {
        this.records = [];
        for (var i = 0; i < data.rows.length; i++) {
          //sqlDate in SQL DATETIME format ("yyyy-mm-ddThh:mm:ss.ms")
          var sqlDate = data.rows.item(i).STARTDATE.toString();
          var splittedStartDate = this.splitDateTime(sqlDate);
          sqlDate = data.rows.item(i).DURATION.toString();
          //var splittedDuration = this.splitDateTime(sqlDate);

          this.records.push({
            A_L_ID: data.rows.item(i).A_L_ID,
            CONTACTPERSONGIVENNAME: data.rows.item(i).GNCONTACTPERSON,
            CONTACTPERSONSURNAME: data.rows.item(i).SNCONTACTPERSON,
            CUSTOMERGIVENNAME: data.rows.item(i).GNCUSTOMER,
            CUSTOMERSURNAME: data.rows.item(i).SNCUSTOMER,
            STARTDATE: data.rows.item(i).STARTDATE,
            ENDDATE: data.rows.item(i).ENDDATE,
            DURATION: data.rows.item(i).DURATION,
            TITEL: data.rows.item(i).TITEL,
            SID: data.rows.item(i).SID,
            U_ID: data.rows.item(i).U_ID,
            LST_ID: data.rows.item(i).LST_ID,
            DATE: splittedStartDate.sDay + "." + splittedStartDate.sMonth + "." + splittedStartDate.sYear + " - " + splittedStartDate.sHour + ":" + splittedStartDate.sMinute,
            PRJ_ID: data.rows.item(i).PRJ_ID,
            PROJECTNAME: data.rows.item(i).PROJECTNAME,
            RECORDTYPE: data.rows.item(i).RECORDTYPE,
            NOTES: data.rows.item(i).NOTES,
            KLIENT_ID: data.rows.item(i).aKLIENT_ID,
            KND_ID: data.rows.item(i).aKND_ID
          });
        }
      }
    }, (error) => {
      alert("ERROR loading records: " + JSON.stringify(error));
    });

  }

  //Dient zum Laden der Diäteinträge
  initializeDietRecords() {

    this.database.executeSql("SELECT a.A_D_ID, a.KLIENT_ID AS aKLIENT_ID, a.LST_ID, a.KILOMETER AS KILOMETER, a.DATUM_VON AS DATE_FROM, a.Datum_BIS AS DATE_UNTIL, a.KND_ID AS aKND_ID, a.DAUER AS DURATION, a.TITEL as TITLE, a.NOTIZ AS NOTES, a.USER_ID AS aUSER_ID, a.AUFZEICHNUNGSART AS RECORDTYPE, a.SYNCHRONIZED AS SYNC, k.KLIENT_ID, k.KND_ID, k.ZUNAME AS GIVENNAME, k.NAME2 AS SURNAME " +
        "FROM INDIAETEN a, MDKND k " +
        "WHERE a.KLIENT_ID = k.KLIENT_ID AND a.KND_ID = k.KND_ID AND a.SYNCHRONIZED = 0", []).then((data) => {

      if (data.rows.length > 0) {
        this.dietrecords = [];
        for (var i = 0; i < data.rows.length; i++) {
          var sqlDate = data.rows.item(i).DATE_FROM.toString();
          var splittedStartDate = this.splitDateTime(sqlDate);

          this.dietrecords.push({
            A_D_ID: data.rows.item(i).A_D_ID,
            KLIENT_ID: data.rows.item(i).aKLIENT_ID,
            GIVENNAME: data.rows.item(i).GIVENNAME,
            SURNAME: data.rows.item(i).SURNAME,
            RECORDTYPE: data.rows.item(i).RECORDTYPE,
            DATE_FROM: data.rows.item(i).DATE_FROM,
            DATE_FROM2: splittedStartDate.sDay + "." + splittedStartDate.sMonth + "." + splittedStartDate.sYear + ", " + splittedStartDate.sHour + ":" + splittedStartDate.sMinute,
            DATE_UNTIL: data.rows.item(i).DATE_UNTIL,
            KILOMETER: data.rows.item(i).KILOMETER,
            KND_ID: data.rows.item(i).aKND_ID,
            USER_ID: data.rows.item(i).aUSER_ID,
            TITLE: data.rows.item(i).TITLE,
            DURATION: data.rows.item(i).DURATION,
            NOTES: data.rows.item(i).NOTES
          });
        }
      }
    }, (error) => {
      alert("ERROR loading diet records: " + JSON.stringify(error));
    });

  }

  //Dient zur besseren Anzeige von Daten (Plural von Datum)
  //siehe "Sonstige_Codequellen_JM"
  splitDateTime(sqlDate){
    //format of sqlDateArr1[] = ['yyyy','mm','dd hh:mm:ms']
    var sqlDateArray = sqlDate.split("-");
    var sYear = sqlDateArray[0];
    var sMonth = sqlDateArray[1];
    //format of sqlDateArr2[] = ['dd', 'hh:mm:ss.ms']
    var sqlDateArray2 = sqlDateArray[2].split("T");
    var sDay = sqlDateArray2[0];
    //format of sqlDateArr3[] = ['hh','mm','ss.ms']
    var sqlDateArray3 = sqlDateArray2[1].split(":");
    var sHour = sqlDateArray3[0];
    var sMinute = sqlDateArray3[1];

    return {sYear:sYear, sMonth:sMonth, sDay: sDay, sHour: sHour, sMinute: sMinute};
  }

  //Löscht eine Leistung
  removeRecord(recordID) {
    this.database.executeSql("DELETE FROM INKNDLST WHERE A_L_ID='" + recordID + "'", []);
    this.initializeRecords();
  }

  //Löscht eine Diät
  removeDietRecord(recordID) {
    this.database.executeSql("DELETE FROM INDIAETEN WHERE A_D_ID='" + recordID + "'", []);
    this.initializeDietRecords();
  }

  //Löscht einen synchronisierten Eintrag
  removeSyncRecord(lstID, lID, dID){
    console.log(typeof(lstID));
    switch(lstID)
    {
      case '0':
        this.database.executeSql("DELETE FROM INDIAETEN WHERE A_D_ID='" + dID + "'", []);
        break;
      default:
        this.database.executeSql("DELETE FROM INKNDLST WHERE A_L_ID='" + lID + "'", []);
        break;
    }
    this.initializeSyncedRecords();
  }

  //Schickt Information einer Leistung an die jeweilige Bearbeitungsseite
  showEdit_l_Modal(ID, GNCON, SNCON, GNCUS, SNCUS, SD, ED, DN, PRJ_ID, TTL, LST_ID, RT, NTS, KLIENT_ID, KND_ID){
    let modal = this.modalCtrl.create(EditLModalPage, {
      ID: ID,
      GNCON: GNCON,
      SNCON: SNCON,
      GNCUS: GNCUS,
      SNCUS: SNCUS,
      SD: SD,
      ED: ED,
      DN: DN,
      PRJ_ID: PRJ_ID,
      TTL: TTL,
      LST_ID: LST_ID,
      RT: RT,
      NTS: NTS,
      KLIENT_ID: KLIENT_ID,
      KND_ID: KND_ID
    });
    modal.onDidDismiss(() => {
      this.initializeRecords();
    });
    modal.present();
  }

  //Schickt Information einer Diät an die jeweilige Bearbeitungsseite
  showEdit_d_Modal(ID, GN, SN, KM, SD, ED, TTL, NTS){
    let modal = this.modalCtrl.create(EditDModalPage, {
      ID: ID,
      GN: GN,
      SN: SN,
      KM: KM,
      SD: SD,
      ED: ED,
      TTL: TTL,
      NTS: NTS
    });
    modal.onDidDismiss(() => {
      this.initializeDietRecords();
    });
    modal.present();
  }

  //Konfiguration und Anzeige des Synchronisieren-Alerts
  showCheckbox() {
    let anzLeistungen=0, anzDiaeten=0, hasRecords=false, hasDiets=false, hasNothing=true;
    //Zählen der Leistungen und Diäten
    this.database.executeSql("SELECT (SELECT COUNT(*) FROM INKNDLST WHERE SYNCHRONIZED = 0) AS LEISTUNGEN, " +
        "(SELECT COUNT(*) FROM INDIAETEN WHERE SYNCHRONIZED = 0) AS DIAETEN, " +
        "(SELECT WEBSERVICE_ENDPOINT FROM MDUSER WHERE U_ID=1) AS URL",[]).then((data)=>{
      if(data.rows.length>0){
        anzLeistungen=data.rows.item(0).LEISTUNGEN;
        if (anzLeistungen > 0){hasRecords=true; hasNothing=false;}
        anzDiaeten=data.rows.item(0).DIAETEN;
        if (anzDiaeten > 0){hasDiets=true; hasNothing=false;}
        this.url=data.rows.item(0).URL;
      }

      /***************************************************************************
       *    Alert: Synchronisieren
       ***************************************************************************/

      this.alert = this.alertCtrl.create();
      this.alert.setTitle('Synchronisieren');
      this.alert.setSubTitle('Was möchten Sie synchronisieren?');

      /**************************************************************************
       *	SendPing
       **************************************************************************/

      if(RequestPing(this.url)==true){
        this.connectionStatus="ist stabil.";
        this.connection=true;
      }
      else{
        this.connectionStatus="ist unterbrochen!";
        this.connection=false;
      }

      this.alert.setSubTitle('Verbindung '+ this.connectionStatus);
      this.alert.addInput({
        type: 'checkbox',
        label: 'Leistungen ('+anzLeistungen+')',
        value: 'l',
        checked: hasRecords
      });

      this.alert.addInput({
        type: 'checkbox',
        label: 'Diäten ('+anzDiaeten+')',
        value: 'd',
        checked: hasDiets
      });

      this.alert.addInput({
        type: 'checkbox',
        label: 'Stammdaten',
        value: 's',
        checked: hasNothing
      });

      if(this.connection){
        this.alert.addButton({
          text: 'Synchronisieren',
          handler: data => {
            console.log('Checkbox data:', data);
            this.syncButtonClicked=true;
            this.testCheckboxResult = data;
            this.presentLoadingCustom();
          }
        });
      }
      this.alert.addButton({
        text:'Abbrechen',
        handler: data => {
          this.syncButtonClicked=false;
        }
      });
      this.alert.onDidDismiss(()=>{
        if(this.syncButtonClicked==true) {
          this.synchronizeData(this.testCheckboxResult, anzLeistungen, anzDiaeten);
          this.loader.dismiss();
        }
      });
      this.alert.present();
    });
  }

  //Synchronisation mit Aufteilung der ausgewählten Daten
  synchronizeData(data, anzLeistungen, anzDiaeten){
    this.SessionLogin();

    for(var i=0; i<data.length;i++) {
      console.log("Ausgewählt: " + data);
      if (data[i] != null) {
        if (data[i] == "l" && anzLeistungen > 0) {
          console.log(data[i]);
          this.createData("leistung");
        }

        if (data[i] == "d" && anzDiaeten > 0) {
          console.log(data[i]);
          this.createData("diaet");
        }

        if (data[i] == "s") {
          console.log(data[i]);
          this.deleteDatafromDb();
          this.ReadData("MDKLTALL");
          this.ReadData("MDKNDALL");
          this.ReadData("MDZHDALL");
          this.ReadData("MDLSTALL");
          this.ReadData("MDPRJALL");
        }
      }
    }

    this.SessionLogout();
    this.initializeSyncedRecords();
  }

  //Startet die Lade-Komponente zu Beginn einer Synchronisation
  presentLoadingCustom() {
    this.loader = this.loadingCtrl.create({
      content: '<div class="custom-spinner-container"'+
      '<div class="custom-spinner-box">Synchronisiere...</div>'+
      '</div>'
    });
    this.loader.present();
  }

  //Diet zum Anmelden am Webservice
  SessionLogin(){
    this.sessionValue=RequestLogin(this.url, this.db_id, this.user, this.pwd, this.klient_id, this.owner, this.ownerPwd);
    console.log("SessionValue: "+this.sessionValue);
  }

  //Laden der Benutzerdaten
  LoadMDUSERData(){

    /* GET LOGIN DATA FROM EINSTELLUNGEN_ACCOUNT */
    this.database.executeSql("SELECT KLIENT_ID, USER_ID, DATABASE_ID, USERNAME, PASSWORD, WEBSERVICE_ENDPOINT, OWNER, OWNER_PASSWORD, DEVICENUMBER FROM MDUSER WHERE U_ID=1", []).then((userdata) => {
      if (userdata.rows.length > 0) {
        for (var i = 0; i < userdata.rows.length; i++) {
          this.klient_id = userdata.rows.item(i).KLIENT_ID.toString();
          this.url = userdata.rows.item(i).WEBSERVICE_ENDPOINT;
          this.user = userdata.rows.item(i).USERNAME;
          this.db_id= userdata.rows.item(i).DATABASE_ID;
          this.pwd = userdata.rows.item(i).PASSWORD;
          this.owner = userdata.rows.item(i).OWNER;
          this.ownerPwd = userdata.rows.item(i).OWNER_PASSWORD;
        }
        console.log("AufzeichnungenPage - MDUSER DATEN ausgelesen.");
      }
    }, (error) => {
      alert("ERROR while loading MDUSER-Daten: " + JSON.stringify(error));
    });
  }

  //Dient zum Abmelden vom Webservice
  SessionLogout(){
    var logout = RequestLogout(this.url, this.sessionValue);
    console.log("Ausgeloggt: "+logout);
  }

  //Dient zum Einspeichern der Stammdaten
  ReadData(command){
    var returnObj = RequestReadData(this.url, this.sessionValue, command);
    var mdData={};
    switch (command){
      case 'MDKLTALL':
        for(var i=0; i<returnObj.ad.length; i++){
          for(var j=0; j<returnObj.ad[i].length; j++){
            mdData[returnObj.f[j]]=returnObj.ad[i][j];
          }
          this.database.executeSql("INSERT INTO MDKLT (KLIENT_ID, BEZEICHNUNG) VALUES (?,?)",[mdData["KLIENT_ID"],mdData["MATCHCODE"]]).then((data) => {
          }, (error) => {
            alert("ERROR : " + JSON.stringify(error));
          });
        }
        console.log("MDKLTALL erfolgreich gespeichert");
        break;
      case 'MDKNDALL':
        for(var i=0; i<returnObj.ad.length; i++){
          for(var j=0; j<returnObj.ad[i].length; j++){
            mdData[returnObj.f[j]]=returnObj.ad[i][j];
          }
          this.database.executeSql("INSERT INTO MDKND (KND_ID, KLIENT_ID, MATCH_KND, ZUNAME, NAME2, ORT, STRASSE, PLZ, TELEFON) VALUES (?,?,?,?,?,?,?,?,?)",[mdData["KND_ID"],mdData["KLIENT_ID"],mdData["MATCHCODE"],mdData["VORNAME"],mdData["ZUNAME"],mdData["ORT"],mdData["STRASSE"],mdData["PLZ"],mdData["TELEFON"]]).then((data) => {
          }, (error) => {
            alert("ERROR : " + JSON.stringify(error));
          });
        }
        console.log("MDKNDALL erfolgreich gespeichert");
        break;
      case 'MDZHDALL':
        for(var i=0; i<returnObj.ad.length; i++){
          for(var j=0; j<returnObj.ad[i].length; j++){
            mdData[returnObj.f[j]]=returnObj.ad[i][j];
          }
          this.mdzhdcounter++;
          this.database.executeSql("INSERT INTO MDZHD (SID, VORNAME, ZUNAME, EMAIL, MOBIL, MOBIL_PRIVAT, KND_ID, KLIENT_ID) VALUES (?,?,?,?,?,?,?,?)",[mdData["SID"], mdData["VORNAME"],mdData["ZUNAME"],mdData["EMAIL"],mdData["MOBIL"],mdData["MOBIL_PRIVAT"],mdData["KND_ID"],mdData["KLIENT_ID"]]).then((data) => {
          }, (error) => {
            console.log("ERROR : " + JSON.stringify(error));
          });
        }
        console.log("MDZHDALL erfolgreich gespeichert");
        break;
      case 'MDLSTALL':
        for(var i=0; i<returnObj.ad.length; i++){
          for(var j=0; j<returnObj.ad[i].length; j++){
            mdData[returnObj.f[j]]=returnObj.ad[i][j];
          }
          this.mdlstcounter++;
          this.database.executeSql("INSERT INTO MDLST (LST_ID, BEZEICHNUNG, KLIENT_ID) VALUES (?,?,?)",[this.mdlstcounter, mdData["BEZEICHNUNG1"],mdData["KLIENT_ID"]]).then((data) => {
          }, (error) => {
            alert("ERROR : " + JSON.stringify(error));
          });
        }
        console.log("MDLST erfolgreich gespeichert");
        break;
      case 'MDPRJALL':
        for(var i=0; i<returnObj.ad.length; i++){
          for(var j=0; j<returnObj.ad[i].length; j++){
            mdData[returnObj.f[j]]=returnObj.ad[i][j];
          }
          this.database.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES (?,?,?,?)",[mdData["PRJ_ID"],mdData["KLIENT_ID"],mdData["BEZEICHNUNG1"],mdData["KND_ID"]]).then((data) => {
          }, (error) => {
            console.log("ERROR : " + JSON.stringify(error));
          });
        }
        console.log("MDPRJ erfolgreich gespeichert");
        break;
    }
  }

  //Dient zum Erstellen der "Sende"-Daten und zum Senden von diesen
  createData(command){
    var createData = new Array();
    var fields = new Array ("SID","KLIENT_ID","KND_ID","PRJ_ID","VTR_ID","LST_ID","LST_VON","LST_BIS","LST_GESAMT","LST_VERRECH","WEGZEIT","WEGZEIT_VERRECH","WEGSTRECKE","WEGSTRECKE_VERRECH","ABSCHLUSS_KZ","FAKT_ID","VORLAGEDATUM","NOTIZ","DATE_CREATED","DATE_MODIFIED","MODIFIED_BY","CREATED_BY","MDZHD_SID","MDADR_SID","MDPERS_SID","LIE_ID","MDKAMDA_SID","LFZ_MIN","BAKT_ID","SUBJECT","USER_ID","DAUER","ROWID","STSCOL_THIS");
    var id_l = new Array();
    var id_d = new Array();

    switch(command){
      case 'leistung':
        for(var i=0; i<this.records.length;i++){
          createData[i] = new Array('',this.records[i]["KLIENT_ID"],this.records[i]["KND_ID"],this.records[i]["PRJ_ID"],'',this.records[i]["LST_ID"],new Date(this.records[i]["STARTDATE"]).getTime(), new Date(this.records[i]["ENDDATE"]).getTime(), '','', '', '','', '', '', '', '', this.records[i]["NOTES"],'', '', '', '',this.records[i]["SID"], '', '', '', '', '', '', this.records[i]["TITEL"], this.records[i]["U_ID"], new Date(this.records[i]["DURATION"]).getTime(), '', '');
          id_l[i] = this.records[i]["A_L_ID"];
        }
        this.setSynchronized("leistung", id_l);
        break;
      case 'diaet':
        for(var i = 0; i < this.dietrecords.length; i++)
        {
          createData[i] = new Array('', this.dietrecords[i]["KLIENT_ID"], this.dietrecords[i]["KND_ID"],'','', this.dietrecords[i]["LST_ID"], new Date(this.dietrecords[i]["DATE_FROM"]).getTime(),  new Date(this.dietrecords[i]["DATE_UNTIL"]).getTime(), '', '', '', '',this.dietrecords[i]["KILOMETER"], '', '', '', '', this.dietrecords[i]["NOTES"], '', '', '', '', '', '', '', '', '', '', '', this.dietrecords[i]["TITLE"], this.dietrecords[i]["U_ID"], new Date(this.dietrecords[i]["DURATION"]).getTime(), '', '');
          id_d[i] = this.dietrecords[i]["A_D_ID"];
        }
        this.setSynchronized("diaet", id_d);
        break;
    }
    var createDataObj ={"config" : {"fields" : fields},
      "data" :  createData
    };

    console.log("createDataObj: "+JSON.stringify(createDataObj));
    var answer = RequestCreateData(this.url, this.sessionValue, "MDKNDLST_SYNCALL", JSON.stringify(createDataObj));
    console.log(answer);
  }

  //Setzt Leistungen und/oder Diäten nach dem Senden auf "synchronisiert"
  setSynchronized(command, id)
  {
    switch(command) {
      case 'leistung':

        for (var i = 0; i < id.length; i++) {
          var updatequery = "UPDATE INKNDLST SET SYNCHRONIZED=? WHERE A_L_ID=?";
          this.database.executeSql(updatequery, [this.synchronized, id[i]]).then((data) => {
            this.records.pop();
          }, (error) => {
            alert("ERROR while setting synchornized and synchronized_date at table INKNDLST: " + JSON.stringify(error));
          });
        }

        break;
      case 'diaet':

        for (var i = 0; i < id.length; i++) {
          var updatequery = "UPDATE INDIAETEN SET SYNCHRONIZED=? WHERE A_D_ID=?";
          this.database.executeSql(updatequery, [this.synchronized, id[i]]).then((data) => {
            this.dietrecords.pop();
          }, (error) => {
            alert("ERROR while setting synchornized and synchronized_date at table INDIAETEN: " + JSON.stringify(error));
          });
        }

        break;
    }
  }

  //Dient zum Löschen des Datenbestandes, bevor die neuen Stammdaten eingespeichert werden
  deleteDatafromDb(){
    this.database.transaction(function (tx) {
      tx.executeSql('DELETE FROM MDKLT');
      tx.executeSql('DELETE FROM MDKND');
      tx.executeSql('DELETE FROM MDZHD');
      tx.executeSql('DELETE FROM MDLST');
      tx.executeSql('DELETE FROM MDPRJ');
      this.mdlstcounter = 0;
      this.mdzhdcounter = 0;
    });
  }
}