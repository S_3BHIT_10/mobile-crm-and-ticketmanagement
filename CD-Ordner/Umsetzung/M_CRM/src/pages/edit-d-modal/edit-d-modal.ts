import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ViewController, AlertController } from 'ionic-angular';
import { SQLite } from "ionic-native";

/*
  Generated class for the EditDModal page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-edit-d-modal',
  templateUrl: 'edit-d-modal.html'
})
export class EditDModalPage {
  database: SQLite;
  a_d_id: any;
  givenname: any;
  surname: any;
  startdate: any;
  enddate: any;
  kilometer: any;
  title: any;
  notes: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public viewCtrl: ViewController, public alertCtrl: AlertController) {
    this.a_d_id = navParams.get("ID");
    this.givenname = navParams.get("GN");
    this.surname = navParams.get("SN");
    this.kilometer = navParams.get("KM");
    this.startdate = navParams.get("SD");
    this.enddate = navParams.get("ED");
    this.title = navParams.get("TTL");
    this.notes = navParams.get("NTS");

    this.platform.ready().then(() => {
      this.database = new SQLite();
      this.database.openDatabase({name: "data.db", bgType: '1', iosDatabaseLocation: 'Library'}).then(() => {
      }, (error) => {
        console.log("ERROR: ", error);
      });

    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditDModalPage');
  }

  //Schließt die Bearbeitungsseite
  dismiss() {
    this.viewCtrl.dismiss();
  }

  //Dient zum Update einer Diät
  updateDRecord()
  {
    if(this.kilometer != '')
    {
      var updatequery = "UPDATE INDIAETEN SET KILOMETER=?, DATUM_VON=?, DATUM_BIS=?, TITEL=?, NOTIZ=? WHERE A_D_ID=?";
      this.database.executeSql(updatequery, [this.kilometer, this.startdate, this.enddate, this.title, this.notes, this.a_d_id]).then((data) => {
        this.dismiss();
      }, (error) => {
        alert("ERROR while updating: " + JSON.stringify(error));
      });
    }
    else
    {
      let alert = this.alertCtrl.create();
      alert.setTitle('Fehler beim Einspeichern!');
      alert.setMessage('Bitte füllen Sie alle relevanten Felder aus! (Kilometer)');
      alert.addButton('Verstanden');
      alert.present();
    }
  }
}
