import { Component, ViewChild} from '@angular/core';
import {NavController, NavParams, Platform, AlertController, Searchbar} from 'ionic-angular';
import {SQLite} from "ionic-native";
import {StammdatenInfoPage} from "../stammdaten-info/stammdaten-info";

/*
  Generated class for the Stammdaten page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-stammdaten',
  templateUrl: 'stammdaten.html'
})

export class StammdatenPage {

  @ViewChild('searchbar') searchbar: Searchbar;

  //DB
  database: SQLite;
  //Stammdaten
  Stammdaten: string = "Ansprechpersonen";
  contactperson: Object[];
  customers: Object[];
  contacpersonsAll: Object[];
  customersAll: Object[];
  customerallcontactperson:Object[];
  searchString: String='';
  searchString2: String='';

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public alertCtrl: AlertController) {
    this.platform.ready().then(() => {
      this.database = new SQLite();
      this.database.openDatabase({name: "data.db", bgType: '1', iosDatabaseLocation: 'Library'}).then(() => {
          this.initializeContactpersons();
          this.initializeCustomers();
      }, (error) => {
        console.log("ERROR: ", error);
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StammdatenPage');
  }

  //Lädt alle Informationen von allen Ansprechperson
  initializeContactpersons() {
    this.database.executeSql("SELECT z.SID, z.VORNAME AS CONTACTPERSONGIVENNAME, z.ZUNAME AS CONTACTPERSONSURNAME, z.EMAIL, z.MOBIL, z.MOBIL_PRIVAT, z.KND_ID, z.KLIENT_ID, k.KND_ID, k.KLIENT_ID, k.ZUNAME AS CUSTOMERGIVENNAME, k.NAME2 AS CUSTOMERSURNAME " +
        " FROM MDZHD z, MDKND k " +
        " WHERE z.KLIENT_ID = k.KLIENT_ID AND z.KND_ID = k.KND_ID " +
        " ORDER BY z.VORNAME", []).then((data) => {
      this.contactperson = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          this.contactperson.push({
            SID: data.rows.item(i).SID,
            CONTACTPERSONGIVENNAME: data.rows.item(i).CONTACTPERSONGIVENNAME,
            CONTACTPERSONSURNAME: data.rows.item(i).CONTACTPERSONSURNAME,
            EMAIL: data.rows.item(i).EMAIL,
            MOBIL: data.rows.item(i).MOBIL,
            MOBIL_PRIVAT: data.rows.item(i).MOBIL_PRIVAT,
            KLIENT_ID: data.rows.item(i).KLIENT_ID,
            CUSTOMERGIVENNAME: data.rows.item(i).CUSTOMERGIVENNAME,
            CUSTOMERSURNAME: data.rows.item(i).CUSTOMERSURNAME
          });
        }
        this.contacpersonsAll = this.contactperson;
      }
    }, (error) => {
      alert("ERROR loading contactpersons: " + JSON.stringify(error));
    });
  }

  //Lädt alle Information von allen Kunden
  initializeCustomers() {
    this.database.executeSql("SELECT KLIENT_ID, KND_ID, MATCH_KND, ZUNAME AS CUSTOMERGIVENNAME, NAME2 AS CUSTOMERSURNAME, ORT, STRASSE, PLZ, TELEFON " +
        " FROM MDKND " +
        " ORDER BY ZUNAME", []).then((data) => {
      this.customers = [];
        if (data.rows.length > 0) {
          for (var i = 0; i < data.rows.length; i++) {
            this.customers.push({
              KLIENT_ID: data.rows.item(i).KLIENT_ID,
              KND_ID: data.rows.item(i).KND_ID,
              MATCH_KND: data.rows.item(i).MATCH_KND,
              CUSTOMERGIVENNAME: data.rows.item(i).CUSTOMERGIVENNAME,
              CUSTOMERSURNAME: data.rows.item(i).CUSTOMERSURNAME,
              ORT: data.rows.item(i).ORT,
              STRASSE: data.rows.item(i).STRASSE,
              PLZ: data.rows.item(i).PLZ,
              TELEFON: data.rows.item(i).TELEFON
            });
          }
          this.customersAll = this.customers;
        }
    }, (error) => {
      alert("ERROR loading customers: " + JSON.stringify(error));
    });
  }

  //Suchfunktion für Searchbar
  getItems(ev: any) {
      if(this.Stammdaten === 'Ansprechpersonen')
      {
        // Reset items back to all of the items
        this.contactperson = this.contacpersonsAll;

        // set val to the value of the searchbar
        let val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
          this.contactperson = this.contactperson.filter((item) => {
            this.searchString = item["CONTACTPERSONGIVENNAME"] + item["CONTACTPERSONSURNAME"];
            return (this.searchString.toLowerCase().indexOf(val.toLowerCase()) > -1);
          })
        }
      }
      else
      {
        // Reset items back to all of the items
        this.customers = this.customersAll;

        // set val to the value of the searchbar
        let val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
          this.customers = this.customers.filter((item) => {
            this.searchString=item["CUSTOMERGIVENNAME"]+item["CUSTOMERSURNAME"] + item["MATCH_KND"];
            return (this.searchString.toLowerCase().indexOf(val.toLowerCase()) > -1);
          })
        }
      }
  }

  /*
    Folgende zwei Funktionen setzen die Eingabe
    der Searchbar wieder zurück auf Nichts
   */
  clearSearchbarContactperson(ev)
  {
      //set searchStrings to ''
      this.searchString = '';
      this.searchString2 = '';

      //set value of search bar to ''
      ev.target.value = '';

      //reload all customers/contactpersons
      this.customers = this.customersAll;
      //this.contactperson = this.contacpersonsAll;
  }
  clearSearchbarCustomer(ev)
  {
      //set searchStrings to ''
      this.searchString = '';
      this.searchString2 = '';

      //set value of search bar to ''
      ev.target.value = '';

      //reload all customers/contactpersons
      //this.customers = this.customersAll;
      this.contactperson = this.contacpersonsAll;
  }

  //Übergibt alle wichtigen Funktion an die "stammdaten-info"-Page für die weitere Anzeige
  goToStammdateninfo(item:string)
  {
    if(!(this.Stammdaten === 'Ansprechpersonen'))
    {
      this.navCtrl.push(StammdatenInfoPage, {
        bez: 'CUS',
        knd_id: item["KND_ID"],
        customergivenname: item["CUSTOMERGIVENNAME"],
        customersurname: item["CUSTOMERSURNAME"],
        match: item["MATCH_KND"],
        ort: item["ORT"],
        strasse: item["STRASSE"],
        plz: item["PLZ"],
        telefon: item["TELEFON"],
        klient_id: item["KLIENT_ID"]
      });
    }
    else {
      this.navCtrl.push(StammdatenInfoPage, {
        bez: 'CON',
        sid: item["SID"],
        contactpersongivenname: item["CONTACTPERSONGIVENNAME"],
        contactpersonsurname: item["CONTACTPERSONSURNAME"],
        email: item["EMAIL"],
        mobil: item["MOBIL"],
        mobil_privat: item["MOBIL_PRIVAT"],
        customergivenname: item["CUSTOMERGIVENNAME"],
        customersurname: item["CUSTOMERSURNAME"],
        klient_id: item["KLIENT_ID"]
      });
    }
  }
}
