import { Component } from '@angular/core';
import {NavController, AlertController, PopoverController, ViewController, Platform} from 'ionic-angular';
import {SQLite} from "ionic-native";

/*
  Generated class for the HomePopover page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-home-popover',
  templateUrl: 'home-popover.html'
})
export class HomePopoverPage {
  database:SQLite;
  constructor(public platform:Platform, public navCtrl:NavController, public alertCtrl:AlertController, public popoverCtrl: PopoverController, public viewCtrl:ViewController) {
    this.platform.ready().then(() => {
      this.database = new SQLite();
      this.database.openDatabase({name: "data.db", bgType: '1', iosDatabaseLocation: 'Library'}).then(() => {
      }, (error) => {
        console.log("Database ERROR: ", error);
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePopoverPage');
  }

  /*
  Folgende Funktion schließen das Popover,
  wodurch weitere ein "onDidDismiss"-Event
  aufgerufen wird und den Benutzer auf die
  jeweilige Page weiterleitet.
   */

  close(){
    this.viewCtrl.dismiss();
  }

  goToAccountEinstellungen(){
    this.viewCtrl.dismiss("EinstellungenAccountPage");
  }

  goToZeitEinstellungen(){
    this.viewCtrl.dismiss("EinstellungenZeitPage");
  }

  DeleteEinstellungen(){
    this.viewCtrl.dismiss("DeleteEinstellungen");
  }
}
