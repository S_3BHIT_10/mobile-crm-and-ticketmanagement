import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import {StatusBar, Splashscreen} from 'ionic-native';
import { HomePage } from '../pages/home/home';
import { SQLite } from 'ionic-native';
import {EinstellungenAccountPage} from "../pages/einstellungen-account/einstellungen-account";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  public mydate: String = new Date().toISOString();

  constructor(platform: Platform) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();

      let db = new SQLite();
      db.openDatabase({
        name: 'data.db',
        bgType: '1', //for background processing database
        iosDatabaseLocation: 'Library'
      }).then(() => {
        //Transaktion starten
        db.transaction(function (tx) {
          //Tabellen löschen, falls vorhanden (TESTZWECKE)
          /*
          tx.executeSql('DROP TABLE IF  EXISTS MDKLT');
          tx.executeSql('DROP TABLE IF  EXISTS MDUSER');
          tx.executeSql('DROP TABLE IF  EXISTS MDKND');
          tx.executeSql('DROP TABLE IF  EXISTS MDZHD');
          tx.executeSql('DROP TABLE IF  EXISTS MDLST');
          tx.executeSql('DROP TABLE IF  EXISTS MDPRJ');
          tx.executeSql('DROP TABLE IF  EXISTS INKNDLST');
          tx.executeSql('DROP TABLE IF  EXISTS INDIAETEN');
          */

          tx.executeSql('CREATE TABLE IF NOT EXISTS MDKLT(KLIENT_ID INTEGER NOT NULL PRIMARY KEY, BEZEICHNUNG TEXT)');
          tx.executeSql('CREATE TABLE IF NOT EXISTS MDUSER(U_ID INTEGER NOT NULL PRIMARY KEY,USER_ID INTEGER NOT NULL, DATABASE_ID TEXT NOT NULL, USERNAME TEXT, PASSWORD TEXT, WEBSERVICE_ENDPOINT TEXT, OWNER TEXT, OWNER_PASSWORD TEXT, DEVICENUMBER TEXT, PERFORMANCEDURATION DATETIME, DIETDURATION DATETIME, SAVEDAYS INTEGER, KLIENT_ID INTEGER NOT NULL, FOREIGN KEY(KLIENT_ID) REFERENCES MDKLT(KLIENT_ID))');
          tx.executeSql('CREATE TABLE IF NOT EXISTS MDKND(KND_ID INTEGER NOT NULL, KLIENT_ID INTEGER NOT NULL, MATCH_KND TEXT, ZUNAME TEXT, NAME2 TEXT, ORT TEXT, STRASSE TEXT, PLZ TEXT, TELEFON TEXT, FOREIGN KEY(KLIENT_ID) REFERENCES MDKLT(KLIENT_ID), PRIMARY KEY(KLIENT_ID, KND_ID))');
          tx.executeSql('CREATE TABLE IF NOT EXISTS MDLST(LST_ID INTEGER NOT NULL PRIMARY KEY, BEZEICHNUNG TEXT, KLIENT_ID INTEGER NOT NULL, FOREIGN KEY(KLIENT_ID) REFERENCES MDKLT(KLIENT_ID))');
          tx.executeSql('CREATE TABLE IF NOT EXISTS MDPRJ(PRJ_ID INTEGER NOT NULL, KLIENT_ID INTEGER NOT NULL, BEZEICHNUNG TEXT, KND_ID INTEGER NOT NULL, FOREIGN KEY(KLIENT_ID) REFERENCES MDKLT(KLIENT_ID), FOREIGN KEY(KND_ID) REFERENCES MDKND(KND_ID), PRIMARY KEY(PRJ_ID, KLIENT_ID))');
          tx.executeSql('CREATE TABLE IF NOT EXISTS MDZHD(SID INTEGER NOT NULL PRIMARY KEY, VORNAME TEXT, ZUNAME TEXT, EMAIL TEXT, MOBIL TEXT, MOBIL_PRIVAT TEXT, KND_ID INTEGER NOT NULL, KLIENT_ID INTEGER NOT NULL, FOREIGN KEY(KND_ID) REFERENCES MDKND(KND_ID), FOREIGN KEY(KLIENT_ID) REFERENCES MDKND(KLIENT_ID))');
          tx.executeSql('CREATE TABLE IF NOT EXISTS INKNDLST(A_L_ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, KLIENT_ID INTEGER NOT NULL, KND_ID INTEGER NOT NULL, SID INTEGER NOT NULL, LST_ID INTEGER NOT NULL, PRJ_ID INTEGER, DATUM_VON DATETIME, DATUM_BIS DATETIME, DAUER INTEGER, TITEL TEXT, NOTIZ TEXT, USER_ID INTEGER NOT NULL, AUFZEICHNUNGSART TEXT NOT NULL, SYNCHRONIZED INTEGER, FOREIGN KEY(KLIENT_ID) REFERENCES MDKLT(KLIENT_ID))');
          tx.executeSql('CREATE TABLE IF NOT EXISTS INDIAETEN(A_D_ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, KLIENT_ID INTEGER NOT NULL, LST_ID TEXT, KILOMETER INTEGER, DATUM_VON DATETIME, DATUM_BIS DATETIME, KND_ID INTEGER NOT NULL, DAUER INTEGER, TITEL TEXT, NOTIZ TEXT, USER_ID INTEGER NOT NULL, AUFZEICHNUNGSART TEXT NOT NULL, SYNCHRONIZED INTEGER, FOREIGN KEY(KLIENT_ID) REFERENCES MDKLT(KLIENT_ID))');

          //TESTDATEN

          /*
          //klient
          tx.executeSql("INSERT INTO MDKLT (KLIENT_ID, BEZEICHNUNG) VALUES (1, 'Multidata')");

          //Standarddauer für Leistungs und Diätseinstellungen generieren
          var dur = new Date(new Date(0).setMinutes(new Date(0).getMinutes()+5));
          var duration = new Date(dur).toISOString();
          //user

          tx.executeSql("INSERT INTO MDUSER (U_ID, USER_ID, DATABASE_ID, USERNAME, PASSWORD, WEBSERVICE_ENDPOINT, OWNER, OWNER_PASSWORD, DEVICENUMBER,PERFORMANCEDURATION, DIETDURATION, SAVEDAYS, KLIENT_ID) VALUES (1, 23,'SUP12','MDDEMO', '', 'http://webservice.multidata.at:50001/WebAppMgrService/WebJson/', 'KREMS', '', 'ab2','"+duration+"','"+duration+"', 0, 1)");

          //kunde
          tx.executeSql("INSERT INTO MDKND (KND_ID, KLIENT_ID, MATCH_KND, ZUNAME, NAME2, ORT, STRASSE, PLZ, TELEFON) VALUES (1, 1, 'JM', 'Julian', 'Meyerhofer', 'Langschlag', 'Neusiedlung', '3921', '0664')");
          tx.executeSql("INSERT INTO MDKND (KND_ID, KLIENT_ID, MATCH_KND, ZUNAME, NAME2, ORT, STRASSE, PLZ, TELEFON) VALUES (2, 1, 'MP', 'Martin', 'Paukner', 'Langschlag', 'Neusiedlung', '3921', '0664')");
          tx.executeSql("INSERT INTO MDKND (KND_ID, KLIENT_ID, MATCH_KND, ZUNAME, NAME2, ORT, STRASSE, PLZ, TELEFON) VALUES (3, 1, 'RR', 'Roland', 'Rogner', 'Langschlag', 'Neusiedlung', '3921', '0664')");
          tx.executeSql("INSERT INTO MDKND (KND_ID, KLIENT_ID, MATCH_KND, ZUNAME, NAME2, ORT, STRASSE, PLZ, TELEFON) VALUES (4, 1, 'RB', 'Robert', 'Böhm', 'Langschlag', 'Neusiedlung', '3921', '0664')");

          //ansprechperson
          tx.executeSql("INSERT INTO MDZHD (SID, VORNAME, ZUNAME, EMAIL, MOBIL, MOBIL_PRIVAT, KND_ID, KLIENT_ID) VALUES(1, 'Max', 'Mustermann', 'Max.Muster@gmx.at', '12345', '6789', 1,1)");
          tx.executeSql("INSERT INTO MDZHD (SID, VORNAME, ZUNAME, EMAIL, MOBIL, MOBIL_PRIVAT, KND_ID, KLIENT_ID) VALUES(2, 'Paul', 'Huber', 'Max.Muster@gmx.at', '12345', '6789', 2,1)");
          tx.executeSql("INSERT INTO MDZHD (SID, VORNAME, ZUNAME, EMAIL, MOBIL, MOBIL_PRIVAT, KND_ID, KLIENT_ID) VALUES(3, 'Silvia', 'Musterfrau', 'Silvia.Muster@gmx.at', '3222', '9996', 3,1)");
          tx.executeSql("INSERT INTO MDZHD (SID, VORNAME, ZUNAME, EMAIL, MOBIL, MOBIL_PRIVAT, KND_ID, KLIENT_ID) VALUES(4, 'Kurt', 'Muster', 'Silvia.Muster@gmx.at', '3222', '9996', 4,1)");
          tx.executeSql("INSERT INTO MDZHD (SID, VORNAME, ZUNAME, EMAIL, MOBIL, MOBIL_PRIVAT, KND_ID, KLIENT_ID) VALUES(5, 'Thomas', 'Binder', 'Tom.Binder@gmx.at', '12345', '6789', 2,1)");

          //kontaktart
          tx.executeSql("INSERT INTO MDLST (LST_ID, BEZEICHNUNG, KLIENT_ID) VALUES(1,'Telefonat',1)");
          tx.executeSql("INSERT INTO MDLST (LST_ID, BEZEICHNUNG, KLIENT_ID) VALUES(2, 'SMS',1)");

          //projektstamm
          tx.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES(1,1,'Weingartner', 1)");
          tx.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES(2,1,'Wagner', 2)");
          tx.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES(3,1,'Multidata', 3)");
          tx.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES(4,1,'Elch', 4)");
          tx.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES(5,1,'Hirsch', 1)");
          tx.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES(6,1,'Gerti', 2)");
          tx.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES(7,1,'Tutgut', 3)");
          tx.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES(8,1,'Sonnentor', 4)");
         */

        }).then((data) => {
          db.executeSql('SELECT EXISTS(SELECT WEBSERVICE_ENDPOINT FROM MDUSER WHERE U_ID=1) AS RESULT', []).then((mdUserData)=>{
            if(mdUserData.rows.item(0).RESULT > 0) {
              this.rootPage = HomePage;
            }
            else {
              this.rootPage = EinstellungenAccountPage;
            }
          });
        }, (err) => {
          alert('Unable to execute sql: '+ err);
        });
      }, (err) => {
        alert('Unable to open database: '+ err);
      });
    });
  }
}
