------------------- Mobiles CRM & Ticketmanagement -------------------

Sch�ler:
 + Julian Meyerhofer (Projektmitarbeiter)
 + Martin Paukner (Projektleiter)

Inhalt der CD:
 - MDWebkrems01: 
      Testprojekt f�r Webservice von Multidata Software International Betriebs GmbH

 - Umsetzung:
      beinhaltet das Projekt M_CRM
      Das Projekt wird aufgerufen, indem man mittels Kommandozeile (CLI) in das 
      Projektverzeichnis navigiert und mit dem Befehl: "ionic run android" das  
      Projekt ausf�hrt.

 - Verwendete Fotos: 
      enth�lt die Bilder, die f�r die Startseite verwendet wurden (Bild, GIMP-Datei, bearbeitetes Bild)

 - Working:
      beinhaltet alle T�tigkeitsberichte sowie Betreuungsprotokolle

 - Diplomarbeit.pdf
      ist die finale schriftliche Arbeit des Projektteams

 - Sonstige_Codequellen:
      diese beiden Datein beinhalten Code, der f�r die Umsetzung zus�tzlich verwendet wurde, und die dazugeh�rige Quelle