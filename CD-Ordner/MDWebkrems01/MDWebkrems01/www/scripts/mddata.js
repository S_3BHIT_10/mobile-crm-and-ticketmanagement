

/***************************************************************************
* 	RequestPing
****************************************************************************/
function RequestPing(url) {
			    				
	var postdata = {};			
	var responseValue = 1;
	var reqUrl = url + "Ping";
	$.support.cors = true;
		
	$.ajax(
			{
				type: "POST",
				url: reqUrl,
				async : false,
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(postdata),
				dataType: "json",
				crossDomain: true,
				success: function(data)
				{   
					responseValue= data.PingResult;
				},
				error: function(a,b,c)
				{
					console.log(a);
					
				}
			}
	);
	
	return responseValue;
	
}


/***************************************************************************
* 	RequestLogin
*	Prameters:  url 		WebService Endpoint
*			  	db			Database ID
*				user		User
*				pwd			User Password
*				klientId	Klient ID
*				owner 		DB Owner
*				ownerPwd 	Password
****************************************************************************/
function RequestLogin(url, db, user, pwd, klientId, owner, ownerPwd) {
						
		var postdata = {};
		
		postdata["db"] = db; 
		postdata["user"] = user; 
		postdata["pwd"] = pwd; 
		postdata["klientId"] = klientId; 
		postdata["owner"] = owner; 
		postdata["ownerPwd"] = ownerPwd;
		
		var sessionID = '';
		
		var url = url + "Login";
		$.support.cors = true;
		
		$.ajax(
			{
				
				type: "POST",
				url: url,
				async: false,
				
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(postdata),
				dataType: "json",
				crossDomain: true,
				success: function(data)
				{
					sessionId = data.LoginResult;
				},
				error: function(a,b,c)
				{
					console.log(a);
				}
			}
		);
		return sessionId;
	}
	
	
/***************************************************************************
* RequestLogout
***************************************************************************/
function RequestLogout(url, sessionGuid) {
			    				
	var postdata = {};
		
	postdata["sessionGuid"] = sessionGuid; 
		
	var url = url + "Logout";
	$.support.cors = true;
		
	$.ajax(
		{
			type: "POST",
			url: url,
			async: false,
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify(postdata),
			dataType: "json",
			crossDomain: true,
			success: function(data)
			{
				//console.log(data);
			},
			error: function(a,b,c)
			{
				console.log(a);
			}
		}
	);
	return "logout";
}




/***************************************************************************
* RequestReadData
****************************************************************************/
function RequestReadData( url, sessionGuid, commandName) {
			    				
	//var resultReadData= document.getElementById("txbReadDataResult");
	//var resultReadDataPageNr= document.getElementById("txbReadDataPageNr");
	//var resultReadDataComplete= document.getElementById("txbReadDataComplete");
	//var loginResult= document.getElementById("txbLoginResult");
	var postdata = {};
	var resultJSON = {};
	var pkFields = new Array;
	var fields = new Array;
	var allData = new Array;
	var allCopyData = new Array;
	var pageNr = 0;
	var pagesCount = 0;
	
	

	var complete = new Boolean(false);
	
	postdata["sessionGuid"] = sessionGuid; 
	postdata["commandName"] = commandName; 

	postdata["data"] = '{ config: { pageSize: 1000 } }'; 
	
	
	var url = url + "ReadData";
	$.support.cors = true;
	
	while (complete==false && sessionGuid != '' && sessionGuid !='logout')
	{
		$.ajax(
			{
				type: "POST",
				url: url,
				async: false,
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(postdata),
				dataType: "json",
				crossDomain: true,
				success: function(data)
				{
					resultJSON = $.parseJSON(data.ReadDataResult);
					
					complete = (resultJSON.config.complete == true);
					

					pageNr = resultJSON.config.pageNr;
					
					
					if (pageNr == 1)
					{	
						pkFields = resultJSON.config.pkFields;
						fields = resultJSON.config.fields;
					}
					
					var success = resultJSON.config.success;
					var pagesCount = resultJSON.config.pagesCount;
					
					
					if (complete===false)
					{
						if (pageNr > 0)
						{
							//console.log(pageNr);
							var nextPageNr = parseInt(resultReadDataPageNr.value) +1;
							postdata["data"] = '{ config: { pageNr: '+ nextPageNr +'} }';
						}
					}
					
					//resultReadData.value = resultJSON.data;
					for (var i = 0; i < resultJSON.data.length; i++) {
						//console.log(resultJSON.data[i]);
						allData.push(resultJSON.data[i])
						allCopyData.push(resultJSON.data[i])
					}
					
					
					
				},
				error: function(a,b,c)
				{
					console.log(a);
				}
			}
		);
		
		
	}
	return {pkf: pkFields, f: fields, ad: allData, acd: allCopyData}; 
}

/***************************************************************************
* RequestUpdateData
****************************************************************************/
function RequestUpdateData( url, sessionGuid, commandName, updateData) {
			    				
	
	var postdata = {};
	var resultJSON = {};
	
	var complete = new Boolean(false);
	
	postdata["sessionGuid"] = sessionGuid; 
	postdata["commandName"] = commandName; 

	postdata["data"] = JSON.stringify(updateData);
	//postdata["data"] = '{"config":{"fields":["KLIENT_ID","KND_ID","ANREDE","TITEL","VORNAME","ZUNAME","NAME2","NAME3","NAME4","NAME5","MATCHCODE","LAND_ID","ZONE_ID","PLZ","ORT"],"originalData":[["1","200001",null,null,null,"Preisauskunft",null,null,null,null,"WEB-KUNDE","AT",null,null,null]],"data":[["1","200001","Herr",null,null,"Preisauskunft",null,null,null,null,"WEB-KUNDE","AT",null,null,null]]}}';
	//postdata["data"] = postdata["data"].replace(/\\([\s\S])|(")/g,"\\$1$2");
	var url = url + "UpdateData";
	$.support.cors = true;
	
	
		$.ajax(
			{
				type: "POST",
				url: url,
				async: false,
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(postdata),
				dataType: "json",
				crossDomain: true,
				success: function(data)
				{
					resultJSON = $.parseJSON(data.UpdateDataResult);
					console.log ( resultJSON.message);
				},
				error: function(a,b,c)
				{
					console.log(a);
				}
			}
		);
		
		
	
	return 'Success: ' + resultJSON.success + '-' + resultJSON.message; 
}



/***************************************************************************
* RequestCreateData
****************************************************************************/
function RequestCreateData( url, sessionGuid, commandName, createData) {

 var postdata = {};
	var resultJSON = {};
	
	var complete = new Boolean(false);
	
	postdata["sessionGuid"] = sessionGuid; 
	postdata["commandName"] = commandName; 

	postdata["data"] = JSON.stringify(createData);
	var url = url + "CreateData";
	$.support.cors = true;
	
	
		$.ajax(
			{
				type: "POST",
				url: url,
				async: false,
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(postdata),
				dataType: "json",
				crossDomain: true,
				success: function(data)
				{
					resultJSON = $.parseJSON(data.CreateDataResult);
					console.log ( resultJSON.message);
				},
				error: function(a,b,c)
				{
					console.log(a);
				}
			}
		);
		
		
	
	return 'Success: ' + resultJSON.success + '-' + resultJSON.message; 


}