import {Component} from '@angular/core';
import {NavController, AlertController, Platform, NavParams, PopoverController} from 'ionic-angular';
import {LeistungErfassenPage } from '../leistung-erfassen/leistung-erfassen';
import {DiaetKundeAuswaehlenPage} from "../diaet-kunde-auswaehlen/diaet-kunde-auswaehlen";
import {AufzeichnungenPage} from "../aufzeichnungen/aufzeichnungen";
import {EinstellungenAccountPage} from "../einstellungen-account/einstellungen-account";
import {EinstellungenZeitPage} from "../einstellungen-zeit/einstellungen-zeit";
import {StammdatenPage} from "../stammdaten/stammdaten";
import {SQLite, InAppBrowser} from "ionic-native";
import {HomePopoverPage} from "../home-popover/home-popover";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  testCheckboxOpen: boolean;
  testCheckboxResult;
  database: SQLite;
  clicked:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public platform: Platform, public popoverCtrl: PopoverController) {
    this.platform.ready().then(() => {
      this.database = new SQLite();
      this.database.openDatabase({name: "data.db", bgType: '1', iosDatabaseLocation: 'Library'}).then(() => {
      }, (error) => {
        console.log("Database ERROR: ", error);
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Home');
  }

  //startet eine neue Instanz des InAppBrowsers (Plugin)
  //siehe "Sonstige_Codequellen_MP"
  launch()
  {
    this.platform.ready().then(() => {
      new InAppBrowser('http://www.multidata.at/', "_system", "location=true");
    });
  }

  //Konfiguration und Anzeige des Popovers, dass eine Übersicht der Einstellungen hat
  presentPopover(myEvent){
    let popover = this.popoverCtrl.create(HomePopoverPage);
    popover.present(
        {ev: myEvent}
    );
    popover.onDidDismiss(data => {
      switch(data){
        case "EinstellungenAccountPage":
          this.navCtrl.push(EinstellungenAccountPage);
          break;
        case "EinstellungenZeitPage":
          this.navCtrl.push(EinstellungenZeitPage);
          break;
        case "DeleteEinstellungen":
          let confirm = this.alertCtrl.create({
            title: 'Wollen sie wirklich alle Daten löschen?',
            message: 'Es werden alle Stammdaten und Aufzeichnungen gelöscht!',
            buttons: [
              {
                text: 'Nein',
                handler: () => {
                  console.log('Nein clicked');
                }
              },
              {
                text: 'Ja',
                handler: () => {
                  console.log('Ja clicked');
                  this.database.transaction(function (tx) {
                    tx.executeSql('DELETE FROM MDKLT');
                    tx.executeSql('DELETE FROM MDUSER');
                    tx.executeSql('DELETE FROM MDKND');
                    tx.executeSql('DELETE FROM MDZHD');
                    tx.executeSql('DELETE FROM MDLST');
                    tx.executeSql('DELETE FROM MDPRJ');
                    tx.executeSql('DELETE FROM INKNDLST');
                    tx.executeSql('DELETE FROM INDIAETEN');
                  }).then((data)=>{
                    this.navCtrl.setRoot(EinstellungenAccountPage);
                  });
                }
              }
            ]
          });
          confirm.present();
          break;
      }
    });
  }

  /******************************************
  Folgende Funktionen leiten den Benutzer auf entsprechende Seiten weiter
   ******************************************/
  goToLeistungErfassen(){
    this.navCtrl.push(LeistungErfassenPage);
  }
  goToDiaetErfassen(){
    this.navCtrl.push(DiaetKundeAuswaehlenPage);
  }
  goToAufzeichnungenSynchronisieren(){
    this.navCtrl.push(AufzeichnungenPage);
  }
  gotToStammdaten(){
    this.navCtrl.push(StammdatenPage);
  }

  showSpeechbubble(){
    //setInterval( );
    //this.clicked = true;

    this.clicked=true;
    setTimeout(() => {
      this.clicked = false}, 2500
    );
  }
}