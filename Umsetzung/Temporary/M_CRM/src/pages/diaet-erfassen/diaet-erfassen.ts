import { Component } from '@angular/core';
import {NavController, NavParams, Platform, AlertController} from 'ionic-angular';
import {SQLite} from "ionic-native";

/*
  Generated class for the DiaetErfassen page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-diaet-erfassen',
  templateUrl: 'diaet-erfassen.html'
})
export class DiaetErfassenPage {
  public database: SQLite;
  //public startDate: String = new Date(new Date(new Date().setMinutes(new Date().getMinutes()-5)).setHours(new Date().getHours()+1)).toISOString();
  //public endDate: String = new Date(new Date().setHours(new Date().getHours()+1)).toISOString();
  //public startDate: String = new Date(new Date(new Date(new Date().setHours(new Date().getHours()+1)).setMinutes(new Date().getMinutes()-5))).toISOString();
  //public endDate: String = new Date(new Date().setHours(new Date().getHours()+1)).toISOString();
  //date and duration
  //string ist kleingeschrieben da wir für Date.parse unten den einfachen Datentyp benötigen
  public startDate: string;
  public endDate: string;
  public duration:string;

  public kilometer: number;
  //user
  mduser_userid: any;
  //kunde
  customer_klient_id: any;
  //aufzeichnungen
  recordingtype: string='Diaet';
  synchronized: number = 0;
  //title
  title: any;
  //notizen
  public description: String = "Das war eine Fahrt für nix und wieder nix";
  //kunde
  customer_zuname: any;
  customer_name2: any;
  customer_lst_id: string='0';
  customer_knd_id: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public alertCtrl: AlertController) {
    this.customer_knd_id = navParams.get("knd_id");
    this.customer_zuname = navParams.get("zuname");
    this.customer_name2 = navParams.get("name2");
    this.customer_klient_id = navParams.get("klient_id");
    this.platform.ready().then(() => {
      this.database = new SQLite();
      this.database.openDatabase({name: "data.db", location: "default"}).then(() => {
        this.selectUserID();
        this.getStandardDuration();
      }, (error) => {
        console.log("ERROR: ", error);
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DiaetErfassenPage');
  }

  //Es wird die eingespeicherte Dauer aus den Einstellungen geladen
  getStandardDuration(){
    this.database.executeSql("SELECT DIETDURATION FROM MDUSER WHERE U_ID=1", []).then((data) => {
      if (data.rows.length > 0) {
        this.duration=new Date(data.rows.item(0).DIETDURATION).toISOString();
        this.startDate = new Date(new Date(new Date(new Date().setHours(new Date().getHours()+2-(new Date(this.duration).getHours()-1))).setMinutes(new Date().getMinutes()-new Date(this.duration).getMinutes()))).toISOString();
        this.endDate = new Date(new Date().setHours(new Date().getHours()+2)).toISOString();
        console.log("standardDurationDiaeten: "+ new Date(this.duration).toISOString());
      }
    }, (error) => {
      alert("ERROR loading diet records: " + JSON.stringify(error));
    });
  }

  //Selektion der USER_ID
  selectUserID()
  {
    this.database.executeSql("SELECT USER_ID FROM MDUSER WHERE KLIENT_ID ="+this.customer_klient_id, []).then((data) => {
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          this.mduser_userid = data.rows.item(i).USER_ID;
        }
      }
    }, (error) => {
      alert("ERROR capturing user_id: " + JSON.stringify(error));
    });
  }

  //Einspeichern der Diät
  insertData(){
    if(this.kilometer){
      var dur = Date.parse(this.duration);
      var time = new Date(dur).getTime();
      this.duration = new Date(time).toISOString();

      var insertquery = "INSERT INTO INDIAETEN (KLIENT_ID, LST_ID, KILOMETER, DATUM_VON, DATUM_BIS, KND_ID, DAUER, TITEL, NOTIZ, USER_ID, AUFZEICHNUNGSART, SYNCHRONIZED) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
      this.database.executeSql(insertquery, [this.customer_klient_id, this.customer_lst_id, this.kilometer, this.startDate, this.endDate, this.customer_knd_id, this.duration,
                                             this.title, this.description, this.mduser_userid, this.recordingtype, this.synchronized]).then((data) => {
      }, (error) => {
        alert("ERROR: " + JSON.stringify(error));
      });
      this.navCtrl.popToRoot();
    }
    else {
      let failure = this.alertCtrl.create();
      failure.setTitle('Fehler beim Einspeichern!');
      failure.setMessage('Bitte füllen Sie alle relevanten Felder aus! (Kilometer)');
      failure.addButton('Verstanden');
      failure.present();
    }
  }

  //Führt den Benutzer zur Startseite zurück
  goBack(){
    this.navCtrl.popToRoot();
  }
}