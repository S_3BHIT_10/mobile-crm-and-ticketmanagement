import { Component } from '@angular/core';
import { NavController, NavParams, Platform, AlertController} from 'ionic-angular';
import {SQLite} from "ionic-native";
import {HomePage} from "../home/home";

/*
  Generated class for the EinstellungenAccount page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-einstellungen-account',
  templateUrl: 'einstellungen-account.html'
})
export class EinstellungenAccountPage {
  database: SQLite;
  klient_id: any;
  webserviceendpoint: any;
  user_id: any;
  db_id:any;
  username: any;
  password: any;
  ownername: any;
  ownerpw: any;
  devicenumber: any;
  description: String = 'Softwareloesung';
  ifValuesInMDUSER:Boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private platform: Platform, public alertCtrl: AlertController) {
    this.platform.ready().then(() => {
      this.database = new SQLite();
      this.database.openDatabase({name: "data.db", bgType: '1', iosDatabaseLocation: 'Library'}).then(() => {
        this.loadData();
      }, (error) => {
        console.log("ERROR: ", error);
      });

    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EinstellungenAccountPage');
  }

  //Legt einen Klient für die Applikation fest
  insertmdklt()
  {
    var insertquery = "INSERT INTO MDKLT (KLIENT_ID, BEZEICHNUNG) VALUES(?,?)";
    this.database.executeSql(insertquery, [this.klient_id, this.description]).then((data) => {
    }, (error) => {
      alert("ERROR : " + JSON.stringify(error));
    });
  }

  //Lädt die Daten des Benutzers, falls welche vorhanden sind
  loadData()
  {
    //Abfragen ob es überhaupt Daten zum Laden gibt
    this.database.executeSql("SELECT EXISTS(SELECT * FROM MDUSER WHERE U_ID=1) AS RESULT",[]).then((data)=>{
      console.log("Daten: "+data.rows.item(0).RESULT);
      this.ifValuesInMDUSER=true;
      //Wenn ja dann lade diese
      if(data.rows.length>0){
        this.database.executeSql("SELECT KLIENT_ID, USER_ID, DATABASE_ID, USERNAME, PASSWORD, WEBSERVICE_ENDPOINT, OWNER, DEVICENUMBER FROM MDUSER WHERE U_ID=1", []).then((data) => {
          if (data.rows.length > 0) {
            for (var i = 0; i < data.rows.length; i++) {
              this.klient_id = data.rows.item(i).KLIENT_ID;
              this.webserviceendpoint = data.rows.item(i).WEBSERVICE_ENDPOINT;
              this.user_id = data.rows.item(i).USER_ID;
              this.db_id = data.rows.item(i).DATABASE_ID;
              this.username = data.rows.item(i).USERNAME;
              this.password = data.rows.item(i).PASSWORD;
              this.ownername = data.rows.item(i).OWNER;
              this.devicenumber = data.rows.item(i).DEVICENUMBER;
            }
            console.log("Laden erfolgreich");
          }
        }, (error) => {
          alert("ERROR while inserting: " + JSON.stringify(error));
        });
      }
      else {
        console.log("There is no data in MDUSER!");
      }
    });
  }

  //Wenn Benutzer vorhanden werden die Daten erneuert, sonst wird ein neuer Benutzer angelegt
  insertData()
  {
    let alert = this.alertCtrl.create();
    alert.setTitle('Speicherfehler');
    alert.setMessage('Bitte füllen Sie alle relevanten Felder aus! (Webservice Endpoint, Klient_ID, User_ID, Datenbank_ID, Username, Ownername, Devicenumber)');
    alert.addButton('Verstanden');

      var insertquery = "SELECT EXISTS(SELECT * FROM MDUSER WHERE U_ID=1) AS RESULT";
      this.database.executeSql(insertquery, []).then((data) => {
        console.log("Daten speichern: "+data.rows.item(0).RESULT);
        if(data.rows.item(0).RESULT > 0){
          //Überprüfen, ob Felder ausgefüllt sind oder nicht!
          if((this.webserviceendpoint && this.klient_id && this.user_id && this.db_id && this.username && this.ownername && this.devicenumber) != '') {
            insertquery = "UPDATE MDUSER SET USER_ID=?, DATABASE_ID=?, USERNAME=?, PASSWORD=?, WEBSERVICE_ENDPOINT=?, OWNER=?, DEVICENUMBER=?, KLIENT_ID=? WHERE U_ID=1";
            this.database.executeSql(insertquery, [this.user_id, this.db_id, this.username, this.password, this.webserviceendpoint, this.ownername, this.devicenumber, this.klient_id]).then((data) => {
            this.navCtrl.pop();
            }, (error) => {
              alert.present();
            });
          }
          else{
            alert.present();
          }
        }
        else{
          //Überprüfen, ob Felder ausgefüllt sind oder nicht!
          if((this.webserviceendpoint && this.klient_id && this.user_id && this.db_id && this.username && this.ownername && this.devicenumber) != null)
          {
            this.insertmdklt();
            insertquery = "INSERT INTO MDUSER (U_ID, USER_ID, DATABASE_ID, USERNAME, PASSWORD, WEBSERVICE_ENDPOINT, OWNER, OWNER_PASSWORD, DEVICENUMBER, KLIENT_ID) VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?)";
            this.database.executeSql(insertquery, [1,this.user_id, this.db_id, this.username, this.password, this.webserviceendpoint, this.ownername, this.ownerpw, this.devicenumber, this.klient_id]).then((data) => {
              this.navCtrl.setRoot(HomePage);
            }, (error) => {
              alert.present();
            });
          }
          else{
            alert.present();
          }
        }
      });
  }
}
