import { Component } from '@angular/core';
import {NavController, NavParams, Platform, ViewController} from 'ionic-angular';
import {SQLite} from "ionic-native";

/*
  Generated class for the EditLModal page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-edit-l-modal',
  templateUrl: 'edit-l-modal.html',
})
export class EditLModalPage {
  database: SQLite;
  a_l_id:any;
  contactpersongivenname: any;
  contactpersonsurname: any;
  customergivenname: any;
  customersurname: any;
  startdate: any;
  enddate: any;
  duration:any;
  prj_id: any;
  projectname: any;
  contacttype: any;
  recordtype: any;
  notes: any;
  klient_id: any;
  knd_id: any;

  //aufzeichnung_leistung
  recordingtype: any;
  title: any;
  //selectedrecordingtype: String ='Vertrieb';
  //kontaktart
  contactTypeList: any[] = [];
  //selectedContactType: any;
  //projektstamm
  //project_bezeichnung:any;
  //project_prj_id: any;
  project: any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public viewCtrl: ViewController) {
    this.a_l_id = navParams.get("ID");
    this.contactpersongivenname = navParams.get("GNCON");
    this.contactpersonsurname = navParams.get("SNCON");
    this.customergivenname = navParams.get("GNCUS");
    this.customersurname = navParams.get("SNCUS");
    this.startdate = navParams.get("SD");
    this.enddate = navParams.get("ED");
    this.duration = navParams.get("DN");
    this.prj_id = navParams.get("PRJ_ID");
    this.title = navParams.get("TTL");
    this.contacttype = navParams.get("LST_ID");
    this.recordtype = navParams.get("RT");
    this.notes = navParams.get("NTS");
    this.klient_id = navParams.get("KLIENT_ID");
    this.knd_id = navParams.get("KND_ID");

    this.platform.ready().then(() => {
      this.database = new SQLite();
      this.database.openDatabase({name: "data.db", bgType: '1', iosDatabaseLocation: 'Library'}).then(() => {
        this.pushRecordingtype();
        this.captureContactType();
        this.captureProject();
      }, (error) => {
        console.log("ERROR: ", error);
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditLModalPage');
  }

  //Beinhaltet die Aufzeichnungsarten
  pushRecordingtype(){
    this.recordingtype = [];
    this.recordingtype.push({value: 'Ticket', text: 'Ticket'});
    this.recordingtype.push({value: 'Vertrieb', text: 'Vertrieb'});
  }

  //Selektiert die Kontaktarten
  captureContactType() {
    this.database.executeSql("SELECT LST_ID, BEZEICHNUNG FROM MDLST WHERE KLIENT_ID ="+this.klient_id, []).then((data) => {
      //this.selectedContactType = data.rows.item(0).LST_ID;
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          this.contactTypeList.push({LST_ID: data.rows.item(i).LST_ID, BEZEICHNUNG: data.rows.item(i).BEZEICHNUNG});
        }
      }
    }, (error) => {
      alert("ERROR capturing contact type: " + JSON.stringify(error));
    });
  }

  //Selektiert die Projekte eines Kunden
  captureProject() {
    this.database.executeSql("SELECT PRJ_ID, BEZEICHNUNG FROM MDPRJ WHERE KND_ID ="+this.knd_id+" ORDER BY BEZEICHNUNG", []).then((data) => {
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          this.project.push({PRJ_ID: data.rows.item(i).PRJ_ID, BEZEICHNUNG: data.rows.item(i).BEZEICHNUNG});
        }
      }
    }, (error) => {
      alert("ERROR capturing project: " + JSON.stringify(error));
    });
  }

  //Ändert die Kontaktart im UI
  changeContacttype(selectedValue)
  {
    this.contacttype = selectedValue;
  }

  //Ändert das Projekt im UI
  changeProject(selectedValue)
  {
    this.prj_id = selectedValue;

  }

  //Dient zum Updaten der Leistung
  updateData(){
    var start = Date.parse(this.startdate);
    var dur = Date.parse(this.duration);
    var time = new Date(dur).getTime();

    this.enddate=new Date(new Date(start).getTime()+time).toISOString();
    //Dauer in Sekunden
    //this.duration = new Date(time).getTime()/1000;
    this.duration = new Date(time).toISOString();

    var updatequery = "UPDATE INKNDLST SET LST_ID=?, PRJ_ID=?, DATUM_VON=?, DATUM_BIS=?, DAUER=?, TITEL=?, NOTIZ=?, AUFZEICHNUNGSART=? WHERE A_L_ID=?";
    this.database.executeSql(updatequery, [this.contacttype, this.prj_id, this.startdate, this.enddate, this.duration, this.title, this.notes, this.recordtype, this.a_l_id]).then((data) => {
      this.dismiss();
    }, (error) => {
      alert("ERROR while updating: " + JSON.stringify(error));
    });
  }

  //Schließt die Bearbeitungsseite
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
