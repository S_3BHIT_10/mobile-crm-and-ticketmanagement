import { Component } from '@angular/core';
import {NavController, NavParams, Platform} from 'ionic-angular';
import {SQLite} from "ionic-native";
/*
  Generated class for the StammdatenInfo page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-stammdaten-info',
  templateUrl: 'stammdaten-info.html'
})
export class StammdatenInfoPage {

  //database
  database: SQLite;

  //kunde
  knd_id: any;
  match_knd: any;
  ort: any;
  strasse: any;
  plz: any;
  telefon: any;
  contactperson: Object[];

  //ansprechperson
  sid: any;
  email: any;
  mobil: any;
  mobil_privat: any;

  //global
  bezeichnung: any;
  customergivenname: any;
  customersurname: any;
  contactpersongivenname: any;
  contactpersonsurname: any;
  klient_id: any;
  persongivenname: any;
  personsurname: any;
  cgcon: Object[];

  //Im Konstruktor werden die Daten der jeweiligen Ansprechperson oder des Kunden geladen
  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform) {

    this.bezeichnung = navParams.get("bez");

    if(this.bezeichnung === 'CUS')
    {
      this.knd_id = navParams.get("knd_id");
      this.customergivenname = navParams.get("customergivenname");
      this.customersurname = navParams.get("customersurname");
      this.match_knd = navParams.get("match");
      this.ort = navParams.get("ort");
      this.strasse = navParams.get("strasse");
      this.plz = navParams.get("plz");
      this.telefon = navParams.get("telefon");
      this.klient_id = navParams.get("klient_id");
      this.contactpersonsurname = navParams.get("contactpersonsurname");
      this.persongivenname = navParams.get("customergivenname");
      this.personsurname = navParams.get("customersurname");
      this.platform.ready().then(() => {
        this.database = new SQLite();
        this.database.openDatabase({name: "data.db", bgType: '1', iosDatabaseLocation: 'Library'}).then(() => {
          this.database.executeSql("SELECT VORNAME AS GIVENNAME, ZUNAME AS SURNAME FROM MDZHD WHERE KND_ID ="+this.knd_id, []).then((data) => {
            this.contactperson = [];
            if (data.rows.length > 0) {
              for (var i = 0; i < data.rows.length; i++) {
                this.contactperson.push({
                  GIVENNAME: data.rows.item(i).GIVENNAME,
                  SURNAME: data.rows.item(i).SURNAME
                });
              }
            }
          }, (error) => {
            alert("ERROR capturing KND_ID: " + JSON.stringify(error));
          });
        }, (error) => {
          console.log("ERROR: ", error);
        });
      });
    }

    if(this.bezeichnung === 'CON')
    {
      this.sid = navParams.get("sid");
      this.contactpersongivenname = navParams.get("contactpersongivenname");
      this.contactpersonsurname = navParams.get("contactpersonsurname");
      this.email = navParams.get("email");
      this.mobil = navParams.get("mobil");
      this.mobil_privat = navParams.get("mobil_privat");
      this.customergivenname = navParams.get("customergivenname");
      this.customersurname = navParams.get("customersurname");
      this.contactpersonsurname = navParams.get("contactpersonsurname");
      this.klient_id = navParams.get("klient_id");
      this.persongivenname = navParams.get("contactpersongivenname");
      this.personsurname = navParams.get("contactpersonsurname");
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StammdatenInfoPage');
  }
}
