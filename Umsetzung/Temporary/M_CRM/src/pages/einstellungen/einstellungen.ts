import { Component } from '@angular/core';
import {NavController, NavParams, AlertController, Platform} from 'ionic-angular';
import {EinstellungenAccountPage} from "../einstellungen-account/einstellungen-account";
import {EinstellungenZeitPage} from "../einstellungen-zeit/einstellungen-zeit";
import {SQLite} from "ionic-native";


/*
  Generated class for the Einstellungen page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-einstellungen',
  templateUrl: 'einstellungen.html'
})
export class EinstellungenPage {
  database:SQLite;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public platform:Platform) {
    this.platform.ready().then(() => {
      this.database = new SQLite();
      this.database.openDatabase({name: "data.db", bgType: '1', iosDatabaseLocation: 'Library'}).then(() => {
      }, (error) => {
        console.log("Database ERROR: ", error);
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EinstellungenPage');
  }

  //Leitet auf die "einstellungen-account"-Page weiter
  goToAccountEinstellungen(){
    this.navCtrl.push(EinstellungenAccountPage);
  }

  //Leitet auf die "einstellungen-zeit"-Page weiter
  goToZeitEinstellungen(){
    this.navCtrl.push(EinstellungenZeitPage);
  }

  //Löscht alle Daten
  DeleteEinstellungen(){

    let confirm = this.alertCtrl.create({
      title: 'Wollen sie wirklich alle Daten löschen?',
      message: 'Es werden alle Stammdaten und Aufzeichnungen gelöscht!',
      buttons: [
        {
          text: 'Nein',
          handler: () => {
            console.log('Nein clicked');
          }
        },
        {
          text: 'Ja',
          handler: () => {
            console.log('Ja clicked');
            this.database.transaction(function (tx) {
              tx.executeSql('DELETE FROM MDKLT');
              tx.executeSql('DELETE FROM MDUSER');
              tx.executeSql('DELETE FROM MDKND');
              tx.executeSql('DELETE FROM MDZHD');
              tx.executeSql('DELETE FROM MDLST');
              tx.executeSql('DELETE FROM MDPRJ');
              tx.executeSql('DELETE FROM INKNDLST');
              tx.executeSql('DELETE FROM INDIAETEN');
            });
          }
        }
      ]
    });
    confirm.present();
  }
}
