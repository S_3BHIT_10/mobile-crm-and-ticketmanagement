import { Component } from '@angular/core';
import {NavController, Platform} from 'ionic-angular';
import {SQLite} from "ionic-native";

/*
  Generated class for the EinstellungenZeit page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-einstellungen-zeit',
  templateUrl: 'einstellungen-zeit.html'
})
export class EinstellungenZeitPage {

  showRecordsTime: number=0;
  standardDurationLeistungen: string;
  standardDurationDiaeten:string;
  database:SQLite;

  constructor(public navCtrl: NavController, public platform: Platform) {
    this.platform.ready().then(() => {
      this.database = new SQLite();
      this.database.openDatabase({name: "data.db", bgType: '1', iosDatabaseLocation: 'Library'}).then(() => {
        this.initializeItems();
      }, (error) => {
        console.log("ERROR: ", error);
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EinstellungenZeitPage');
  }

  //Selektiert die Zeiteinstellungen des Benutzers für die Standarddauer einer Leistung und Diät
  initializeItems(){
    this.database.executeSql("SELECT PERFORMANCEDURATION, DIETDURATION, SAVEDAYS FROM MDUSER WHERE U_ID=1", []).then((data) => {
      if (data.rows.length > 0) {
        this.showRecordsTime=data.rows.item(0).SAVEDAYS;
        this.standardDurationLeistungen=new Date(data.rows.item(0).PERFORMANCEDURATION).toISOString();
        this.standardDurationDiaeten=new Date(data.rows.item(0).DIETDURATION).toISOString();
        console.log("Versuch:"+new Date(this.showRecordsTime).toISOString());
        console.log("standardDurationDiaeten: "+ new Date(this.standardDurationDiaeten).toISOString());
        console.log("standardDurationLeistungen: "+ new Date(this.standardDurationLeistungen).toISOString());
      }
    }, (error) => {
      alert("ERROR loading diet records: " + JSON.stringify(error));
    });
  }

  //Leitet den Benutzer auf die Startseite zurück
  goBack(){
    this.navCtrl.pop();
  }

  //Speichert die neuen Zeiteinstellungen ein
  insertData(){
    var insertquery = "UPDATE MDUSER SET PERFORMANCEDURATION=?, DIETDURATION=?, SAVEDAYS=? WHERE U_ID=1";
    this.database.executeSql(insertquery, [this.standardDurationLeistungen, this.standardDurationDiaeten, this.showRecordsTime]).then((data) => {
    this.navCtrl.popToRoot();
    }, (error) => {
      alert("ERROR: " + JSON.stringify(error));
    });
  }
}
