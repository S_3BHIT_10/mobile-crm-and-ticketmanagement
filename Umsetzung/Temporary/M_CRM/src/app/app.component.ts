import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import {StatusBar, Splashscreen} from 'ionic-native';
import { HomePage } from '../pages/home/home';
import { SQLite } from 'ionic-native';
import {EinstellungenAccountPage} from "../pages/einstellungen-account/einstellungen-account";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  public mydate: String = new Date().toISOString();

  constructor(platform: Platform) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();

      let db = new SQLite();
      db.openDatabase({
        name: 'data.db',
        bgType: '1', //for background processing database
        iosDatabaseLocation: 'Library'
      }).then(() => {
        //Transaktion starten
        db.transaction(function (tx) {
          //Tabellen löschen, falls vorhanden (TESTZWECKE)
          /*
          tx.executeSql('DROP TABLE IF  EXISTS MDKLT');
          tx.executeSql('DROP TABLE IF  EXISTS MDUSER');
          tx.executeSql('DROP TABLE IF  EXISTS MDKND');
          tx.executeSql('DROP TABLE IF  EXISTS MDZHD');
          tx.executeSql('DROP TABLE IF  EXISTS MDLST');
          tx.executeSql('DROP TABLE IF  EXISTS MDPRJ');
          tx.executeSql('DROP TABLE IF  EXISTS INKNDLST');
          tx.executeSql('DROP TABLE IF  EXISTS INDIAETEN');
          */

          tx.executeSql('CREATE TABLE IF NOT EXISTS MDKLT(KLIENT_ID INTEGER NOT NULL PRIMARY KEY, BEZEICHNUNG TEXT)');
          tx.executeSql('CREATE TABLE IF NOT EXISTS MDUSER(U_ID INTEGER NOT NULL PRIMARY KEY,USER_ID INTEGER NOT NULL, DATABASE_ID TEXT NOT NULL, USERNAME TEXT, PASSWORD TEXT, WEBSERVICE_ENDPOINT TEXT, OWNER TEXT, OWNER_PASSWORD TEXT, DEVICENUMBER TEXT, PERFORMANCEDURATION DATETIME, DIETDURATION DATETIME, SAVEDAYS INTEGER, KLIENT_ID INTEGER NOT NULL, FOREIGN KEY(KLIENT_ID) REFERENCES MDKLT(KLIENT_ID))');
          tx.executeSql('CREATE TABLE IF NOT EXISTS MDKND(KND_ID INTEGER NOT NULL, KLIENT_ID INTEGER NOT NULL, MATCH_KND TEXT, ZUNAME TEXT, NAME2 TEXT, ORT TEXT, STRASSE TEXT, PLZ TEXT, TELEFON TEXT, FOREIGN KEY(KLIENT_ID) REFERENCES MDKLT(KLIENT_ID), PRIMARY KEY(KLIENT_ID, KND_ID))');
          tx.executeSql('CREATE TABLE IF NOT EXISTS MDLST(LST_ID INTEGER NOT NULL PRIMARY KEY, BEZEICHNUNG TEXT, KLIENT_ID INTEGER NOT NULL, FOREIGN KEY(KLIENT_ID) REFERENCES MDKLT(KLIENT_ID))');
          tx.executeSql('CREATE TABLE IF NOT EXISTS MDPRJ(PRJ_ID INTEGER NOT NULL, KLIENT_ID INTEGER NOT NULL, BEZEICHNUNG TEXT, KND_ID INTEGER NOT NULL, FOREIGN KEY(KLIENT_ID) REFERENCES MDKLT(KLIENT_ID), FOREIGN KEY(KND_ID) REFERENCES MDKND(KND_ID), PRIMARY KEY(PRJ_ID, KLIENT_ID))');
          tx.executeSql('CREATE TABLE IF NOT EXISTS MDZHD(SID INTEGER NOT NULL PRIMARY KEY, VORNAME TEXT, ZUNAME TEXT, EMAIL TEXT, MOBIL TEXT, MOBIL_PRIVAT TEXT, KND_ID INTEGER NOT NULL, KLIENT_ID INTEGER NOT NULL, FOREIGN KEY(KND_ID) REFERENCES MDKND(KND_ID), FOREIGN KEY(KLIENT_ID) REFERENCES MDKND(KLIENT_ID))');
          tx.executeSql('CREATE TABLE IF NOT EXISTS INKNDLST(A_L_ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, KLIENT_ID INTEGER NOT NULL, KND_ID INTEGER NOT NULL, SID INTEGER NOT NULL, LST_ID INTEGER NOT NULL, PRJ_ID INTEGER, DATUM_VON DATETIME, DATUM_BIS DATETIME, DAUER INTEGER, TITEL TEXT, NOTIZ TEXT, USER_ID INTEGER NOT NULL, AUFZEICHNUNGSART TEXT NOT NULL, SYNCHRONIZED INTEGER, FOREIGN KEY(KLIENT_ID) REFERENCES MDKLT(KLIENT_ID))');
          tx.executeSql('CREATE TABLE IF NOT EXISTS INDIAETEN(A_D_ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, KLIENT_ID INTEGER NOT NULL, LST_ID TEXT, KILOMETER INTEGER, DATUM_VON DATETIME, DATUM_BIS DATETIME, KND_ID INTEGER NOT NULL, DAUER INTEGER, TITEL TEXT, NOTIZ TEXT, USER_ID INTEGER NOT NULL, AUFZEICHNUNGSART TEXT NOT NULL, SYNCHRONIZED INTEGER, FOREIGN KEY(KLIENT_ID) REFERENCES MDKLT(KLIENT_ID))');

          //TESTDATEN
          /*

          //klient
          tx.executeSql("INSERT INTO MDKLT (KLIENT_ID, BEZEICHNUNG) VALUES (1, 'Multidata')");

          //Standarddauer für Leistungs und Diätseinstellungen generieren
          var dur = new Date(new Date(0).setMinutes(new Date(0).getMinutes()+5));
          var duration = new Date(dur).toISOString();
          //user

          tx.executeSql("INSERT INTO MDUSER (U_ID, USER_ID, DATABASE_ID, USERNAME, PASSWORD, WEBSERVICE_ENDPOINT, OWNER, OWNER_PASSWORD, DEVICENUMBER,PERFORMANCEDURATION, DIETDURATION, SAVEDAYS, KLIENT_ID) VALUES (1, 23,'SUP12','MDDEMO', '', 'http://webservice.multidata.at:50001/WebAppMgrService/WebJson/', 'KREMS', '', 'ab2','"+duration+"','"+duration+"', 0, 1)");

          //kunde
          tx.executeSql("INSERT INTO MDKND (KND_ID, KLIENT_ID, MATCH_KND, ZUNAME, NAME2, ORT, STRASSE, PLZ, TELEFON) VALUES (1, 1, 'MD', 'Multidata', 'Software International', 'Groß Gerungs', 'Unterer Marktplatz 12', '3921', '0664633212')");
          tx.executeSql("INSERT INTO MDKND (KND_ID, KLIENT_ID, MATCH_KND, ZUNAME, NAME2, ORT, STRASSE, PLZ, TELEFON) VALUES (2, 1, 'WN', 'Bäcker Wagner', '', 'Groß Gerungs', 'Unterer Marktplatz 66', '3921', '066446554')");
          tx.executeSql("INSERT INTO MDKND (KND_ID, KLIENT_ID, MATCH_KND, ZUNAME, NAME2, ORT, STRASSE, PLZ, TELEFON) VALUES (3, 1, 'EP', 'Electric', 'Power Systems', 'Groß Gerungs', 'Zwettler Straße 29', '3921', '066432123')");
          tx.executeSql("INSERT INTO MDKND (KND_ID, KLIENT_ID, MATCH_KND, ZUNAME, NAME2, ORT, STRASSE, PLZ, TELEFON) VALUES (4, 1, 'MI', 'Menhart Installationen GmbH', '', 'Groß Gerungs', 'Linzer Straße 15', '3920', '066415153')");
          tx.executeSql("INSERT INTO MDKND (KND_ID, KLIENT_ID, MATCH_KND, ZUNAME, NAME2, ORT, STRASSE, PLZ, TELEFON) VALUES (5, 1, 'HI', 'Gasthof Hirsch', '', 'Groß Gerungs', 'Hauptplatz 5', '3920', '066415153')");
          tx.executeSql("INSERT INTO MDKND (KND_ID, KLIENT_ID, MATCH_KND, ZUNAME, NAME2, ORT, STRASSE, PLZ, TELEFON) VALUES (6, 1, 'SP', 'SPAR Supermarkt GmbH', '', 'Groß Gerungs', 'Hauptplatz 38', '3920', '066415153')");
          tx.executeSql("INSERT INTO MDKND (KND_ID, KLIENT_ID, MATCH_KND, ZUNAME, NAME2, ORT, STRASSE, PLZ, TELEFON) VALUES (7, 1, 'UN', 'Unimarkt GmbH', '', 'Groß Gerungs', 'Gröblinger Straße 415', '3920', '066415153')");
          tx.executeSql("INSERT INTO MDKND (KND_ID, KLIENT_ID, MATCH_KND, ZUNAME, NAME2, ORT, STRASSE, PLZ, TELEFON) VALUES (8, 1, 'HO', 'Hofer KG', '', 'Groß Gerungs', 'Kreutzberg 418', '3920', '066415153')");

          //ansprechperson
          tx.executeSql("INSERT INTO MDZHD (SID, VORNAME, ZUNAME, EMAIL, MOBIL, MOBIL_PRIVAT, KND_ID, KLIENT_ID) VALUES(1, 'Liselotte', 'Pulb', 'liselotte.pulb@gmx.at', '06663740181', '028121010', 1,1)");
          tx.executeSql("INSERT INTO MDZHD (SID, VORNAME, ZUNAME, EMAIL, MOBIL, MOBIL_PRIVAT, KND_ID, KLIENT_ID) VALUES(2, 'Herbert', 'Pulb', 'h.pulb@gmx.at', '06663740181', '028121010', 2,1)");
          tx.executeSql("INSERT INTO MDZHD (SID, VORNAME, ZUNAME, EMAIL, MOBIL, MOBIL_PRIVAT, KND_ID, KLIENT_ID) VALUES(3, 'Roland', 'Rogner', 'roland.rogner@multidata.at', '06663740181', '028121010', 3,1)");
          tx.executeSql("INSERT INTO MDZHD (SID, VORNAME, ZUNAME, EMAIL, MOBIL, MOBIL_PRIVAT, KND_ID, KLIENT_ID) VALUES(4, 'Karl', 'Hajek', 'hajek.karl@gmx.at', '06663740181', '028121010', 4,1)");
          tx.executeSql("INSERT INTO MDZHD (SID, VORNAME, ZUNAME, EMAIL, MOBIL, MOBIL_PRIVAT, KND_ID, KLIENT_ID) VALUES(5, 'Rene', 'Konijcek', 'r.konijcek@gmx.at', '06663740181', '028121010', 5,1)");
          tx.executeSql("INSERT INTO MDZHD (SID, VORNAME, ZUNAME, EMAIL, MOBIL, MOBIL_PRIVAT, KND_ID, KLIENT_ID) VALUES(6, 'Julian', 'Zelenka', 'j.zelenka@gmx.at', '06663740181', '028121010', 6,1)");
          tx.executeSql("INSERT INTO MDZHD (SID, VORNAME, ZUNAME, EMAIL, MOBIL, MOBIL_PRIVAT, KND_ID, KLIENT_ID) VALUES(7, 'Max', 'Mustermann', 'm.mustermann@gmx.at', '06663740181', '028121010', 7,1)");
          tx.executeSql("INSERT INTO MDZHD (SID, VORNAME, ZUNAME, EMAIL, MOBIL, MOBIL_PRIVAT, KND_ID, KLIENT_ID) VALUES(8, 'Silvia', 'Musterfrau', 's.musterfrau@gmail.at', '06663740181', '028121010', 8,1)");

          //kontaktart
          tx.executeSql("INSERT INTO MDLST (LST_ID, BEZEICHNUNG, KLIENT_ID) VALUES(1, 'Telefonat',1)");
          tx.executeSql("INSERT INTO MDLST (LST_ID, BEZEICHNUNG, KLIENT_ID) VALUES(2, 'SMS',1)");
          tx.executeSql("INSERT INTO MDLST (LST_ID, BEZEICHNUNG, KLIENT_ID) VALUES(3, 'Skype Konversation',1)");
          tx.executeSql("INSERT INTO MDLST (LST_ID, BEZEICHNUNG, KLIENT_ID) VALUES(4, 'E-Mail',1)");

          //projektstamm
          tx.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES(1,1,'PRJ 512', 1)");
          tx.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES(2,1,'PRJ 106', 2)");
          tx.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES(3,1,'PRJ 222', 3)");
          tx.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES(4,1,'PRJ 69', 4)");
          tx.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES(5,1,'PRJ 366', 1)");
          tx.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES(6,1,'PRJ 203', 2)");
          tx.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES(7,1,'PRJ 377', 3)");
          tx.executeSql("INSERT INTO MDPRJ (PRJ_ID, KLIENT_ID, BEZEICHNUNG, KND_ID) VALUES(8,1,'PRJ 541', 4)");
          */

        }).then((data) => {
          db.executeSql('SELECT EXISTS(SELECT WEBSERVICE_ENDPOINT FROM MDUSER WHERE U_ID=1) AS RESULT', []).then((mdUserData)=>{
            if(mdUserData.rows.item(0).RESULT > 0) {
              this.rootPage = HomePage;
            }
            else {
              this.rootPage = EinstellungenAccountPage;
            }
          });
        }, (err) => {
          alert('Unable to execute sql: '+ err);
        });
      }, (err) => {
        alert('Unable to open database: '+ err);
      });
    });
  }
}
