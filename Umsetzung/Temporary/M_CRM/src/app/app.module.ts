import {NgModule, ErrorHandler} from '@angular/core';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {LeistungErfassenPage} from '../pages/leistung-erfassen/leistung-erfassen';
import {EinstellungenPage} from '../pages/einstellungen/einstellungen';
import {AufzeichnungenPage} from "../pages/aufzeichnungen/aufzeichnungen";
import {DiaetErfassenPage} from "../pages/diaet-erfassen/diaet-erfassen";
import {LeistungLeistungsdetailsErfassenPage} from "../pages/leistung-leistungsdetails-erfassen/leistung-leistungsdetails-erfassen";
import {EinstellungenAccountPage} from "../pages/einstellungen-account/einstellungen-account";
import {EinstellungenZeitPage} from "../pages/einstellungen-zeit/einstellungen-zeit";
import {DiaetKundeAuswaehlenPage} from "../pages/diaet-kunde-auswaehlen/diaet-kunde-auswaehlen";
import {EditLModalPage} from "../pages/edit-l-modal/edit-l-modal";
import {StammdatenPage} from "../pages/stammdaten/stammdaten";
import {EditDModalPage} from "../pages/edit-d-modal/edit-d-modal";
import {StammdatenInfoPage} from "../pages/stammdaten-info/stammdaten-info";
import {HomePopoverPage} from "../pages/home-popover/home-popover";

@NgModule({
  declarations: [
      MyApp,
      HomePage,
      LeistungErfassenPage,
      DiaetErfassenPage,
      AufzeichnungenPage,
      EinstellungenPage,
      LeistungLeistungsdetailsErfassenPage,
      EinstellungenAccountPage,
      EinstellungenZeitPage,
      DiaetKundeAuswaehlenPage,
      EditLModalPage,
      StammdatenPage,
      EditDModalPage,
      StammdatenInfoPage,
      HomePopoverPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
      MyApp,
      HomePage,
      LeistungErfassenPage,
      DiaetErfassenPage,
      AufzeichnungenPage,
      EinstellungenPage,
      LeistungLeistungsdetailsErfassenPage,
      EinstellungenAccountPage,
      EinstellungenZeitPage,
      DiaetKundeAuswaehlenPage,
      EditLModalPage,
      StammdatenPage,
      EditDModalPage,
      StammdatenInfoPage,
      HomePopoverPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
